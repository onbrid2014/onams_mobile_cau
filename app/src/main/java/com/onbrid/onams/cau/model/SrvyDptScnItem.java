package com.onbrid.onams.cau.model;

import android.content.Context;

import com.onbrid.onams.cau.util.OnbridPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Enable on 2016-01-15.
 */
public class SrvyDptScnItem implements Serializable {

    private Context context;

    private String UNIVNO;
    private String CAMPUSNO;
    private String PDANO;
    private String LOGINEMP;
    private String PLANNO;
    private int    ACTIONGUBN;
    private int    READGUBN;
    private int    SURVEYGUBN;
    private String SCANPLACENO;
    private String SCANDEPTNO;
    private String BARCODE;

    public SrvyDptScnItem(Context context) {
        this.context  = context;
        this.UNIVNO   = OnbridPreference.getString(this.context, "univNo");
        this.CAMPUSNO = OnbridPreference.getString(this.context, "campusNo");
        this.PDANO    = OnbridPreference.getString(this.context, "loginEmp");
        this.LOGINEMP = OnbridPreference.getString(this.context, "loginEmp");
        this.ACTIONGUBN = 1; // web (부서조사)
        this.READGUBN   = 4; // 스마트폰 입력
    }

    public JSONObject getScanItem() {
        JSONObject object = new JSONObject();
        try {
            object.put("UNIVNO", this.UNIVNO);
            object.put("CAMPUSNO", this.CAMPUSNO);
            object.put("PDANO", this.PDANO);
            object.put("LOGINEMP", this.LOGINEMP);
            object.put("PLANNO", this.PLANNO);
            object.put("ACTIONGUBN", this.ACTIONGUBN);
            object.put("READGUBN", this.READGUBN);
            object.put("SURVEYGUBN", this.SURVEYGUBN);
            object.put("SCANPLACENO", this.SCANPLACENO);
            object.put("SCANDEPTNO", this.SCANDEPTNO);
            object.put("BARCODE", this.BARCODE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }


    public String getUNIVNO() {
        return UNIVNO;
    }


    public String getCAMPUSNO() {
        return CAMPUSNO;
    }

    public String getPDANO() {
        return PDANO;
    }

    public String getLOGINEMP() {
        return LOGINEMP;
    }

    public String getPLANNO() {
        return PLANNO;
    }

    public void setPLANNO(String PLANNO) {
        this.PLANNO = PLANNO;
    }

    public int getACTIONGUBN() {
        return ACTIONGUBN;
    }

    public void setACTIONGUBN(int ACTIONGUBN) {
        this.ACTIONGUBN = ACTIONGUBN;
    }

    public int getREADGUBN() {
        return READGUBN;
    }

    public void setREADGUBN(int READGUBN) {
        this.READGUBN = READGUBN;
    }

    public int getSURVEYGUBN() {
        return SURVEYGUBN;
    }

    public void setSURVEYGUBN(int SURVEYGUBN) {
        this.SURVEYGUBN = SURVEYGUBN;
    }

    public String getSCANPLACENO() {
        return SCANPLACENO;
    }

    public void setSCANPLACENO(String SCANPLACENO) {
        this.SCANPLACENO = SCANPLACENO;
    }

    public String getSCANDEPTNO() {
        return SCANDEPTNO;
    }

    public void setSCANDEPTNO(String SCANDEPTNO) {
        this.SCANDEPTNO = SCANDEPTNO;
    }

    public String getBARCODE() {
        return BARCODE;
    }

    public void setBARCODE(String BARCODE) {
        this.BARCODE = BARCODE;
    }
}
