package com.onbrid.onams.cau.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Enable on 2016-01-14.
 */
public class OnbridResponseHandler extends JsonHttpResponseHandler {

    private Context context;

    public OnbridResponseHandler(Context context) {
        this.context = context;
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
        try {
            Toast.makeText(context, "통신에 실패 했습니다.", Toast.LENGTH_LONG).show();
            Log.e("errorResponse", errorResponse.toString());
            // super.onFailure(statusCode, headers, throwable, errorResponse);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
        try {
            Toast.makeText(context, "통신에 실패 했습니다.", Toast.LENGTH_LONG).show();
            Log.e("errorArrResponse", errorResponse.toString());
            // super.onFailure(statusCode, headers, throwable, errorResponse);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
        try {
            // Toast.makeText(context, "통신에 실패 했습니다.", Toast.LENGTH_LONG).show();
            Log.e("throwable", throwable.getMessage());
            // super.onFailure(statusCode, headers, responseString, throwable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onFinish() {
        showProgress(false);
        super.onFinish();
    }

    @Override
    public void onStart() {
        showProgress(true);
        super.onStart();
    }

    @Override
    public void onCancel() {
        showProgress(false);
        super.onCancel();
    }


    ProgressDialog progressDialog;
    public void showProgress(boolean show ){

        if( show ){

            try{
                progressDialog = new ProgressDialog( context );
                progressDialog.setCancelable(false);
                progressDialog.setMessage("잠시만 기다려주세요.");
                progressDialog.show();
            }catch(Exception e){

            }


        }else{

            if( progressDialog != null ){
                progressDialog.dismiss();
            }

        }

    }

}
