package com.onbrid.onams.cau.adaptor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.onbrid.onams.cau.R;
import com.onbrid.onams.cau.SurveyDeptScan;
import com.onbrid.onams.cau.model.SrvyDptScnItem;

import java.util.ArrayList;

/**
 * Created by Enable on 2016-01-14.
 */
public class CodeDeleteAdaptor extends ArrayAdapter {

    private Context context;

    public CodeDeleteAdaptor(Context context, ArrayList<SrvyDptScnItem> assetList) {
        super(context, 0, assetList);
        this.context = context;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // return super.getView(position, convertView, parent);
        final SrvyDptScnItem item = (SrvyDptScnItem) getItem(position);
        ViewHoler viewHoler;

        if (convertView == null) {
            viewHoler = new ViewHoler();
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_layout_code_delete, parent, false);

            viewHoler.itemNo = (TextView) convertView.findViewById(R.id.surveyNo);
            viewHoler.delete = (ImageView) convertView.findViewById(R.id.ivDelete);

            convertView.setTag(viewHoler);


        } else {
            viewHoler = (ViewHoler) convertView.getTag();
        }

        viewHoler.itemNo.setText(item.getBARCODE());
        viewHoler.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SurveyDeptScan) context).deleteSurveyNo(position);
            }
        });



        return convertView;
    }


    private static class ViewHoler {
        TextView itemNo;
        ImageView delete;
    }

}
