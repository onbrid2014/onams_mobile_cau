package com.onbrid.onams.cau.dialog;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by Enable on 2015-10-14.
 */
public class OnbridProgressDialog extends ProgressDialog {
    public OnbridProgressDialog(Context context) {
        super(context);

        this.setCancelable(false);
        this.setMessage("잠시만 기다려 주세요.");
    }

    public OnbridProgressDialog(Context context, int theme) {
        super(context, theme);
    }
}
