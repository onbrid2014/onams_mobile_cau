package com.onbrid.onams.cau.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.onbrid.onams.cau.MarketPlusActivity;
import com.onbrid.onams.cau.model.MarketItem;
import com.onbrid.onams.cau.util.OnbridPreference;
import com.onbrid.onams.cau.util.OnbridResponseHandler;
import com.onbrid.onams.cau.R;
import com.onbrid.onams.cau.adaptor.MarketListAdaptor;
import com.onbrid.onams.cau.util.OnbridRestClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Enable on 2016-03-03.
 */
public class MarketAsset extends Fragment {

    private View rootView;

    private LinearLayout underLine;
    private TextView     tvNoData;

    private ListView listView ;
    private ArrayList<MarketItem> marketItems;
    private MarketListAdaptor marketListAdaptor;

    private int currentPage ;
    private int totalPage   ;
    private int pageRow     =   10;
    private boolean loading ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_market_asset, container, false);

        /* enable onCreateOptionsMenu */
        setHasOptionsMenu(true);

        initControll();




        return rootView;
    }

    /**
     * 컨트롤 세팅 이벤트 연결
     */
    private void initControll() {

        listView = (ListView) rootView.findViewById(R.id.listview);

        underLine = (LinearLayout) rootView.findViewById(R.id.underLine);
        tvNoData  = (TextView) rootView.findViewById(R.id.tvNoData);

        clearControll();

        goMarketList();
    }

    private void initViewControll() {
        marketListAdaptor = new MarketListAdaptor(getActivity(), marketItems);
        listView.setAdapter(marketListAdaptor);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Log.e("ddddd", ((MarketItem) marketListAdaptor.getItem(position)).getConfState()+"");
                final MarketItem item = (MarketItem) marketListAdaptor.getItem(position);
                final int posi = position;

                if (((MarketItem) marketListAdaptor.getItem(position)).getConfState() < 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage("신청된 내용을 삭제하겠습니까?");
                    builder.setTitle("삭제");
                    builder.setNegativeButton("삭제", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            deleteMarketAsset(item, posi);
                        }
                    });
                    builder.setPositiveButton("닫기", null);
                    builder.show();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage("이미 처리되어 삭제할 수 없습니다.");
                    builder.setTitle("삭제불가");
                    builder.setPositiveButton("닫기", null);
                    builder.show();
                }
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                System.out.println(scrollState);
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                // System.out.println( "loading : "+ loading +" || " + firstVisibleItem + "/" + visibleItemCount + "/"+ totalItemCount + " || " + currentPage+"/"+totalPage);
                if (!loading && currentPage < totalPage && (firstVisibleItem + visibleItemCount) >= (totalItemCount - 0)) {
                    loading = true;
                    String url = "/ws/am/marketAsset/selectMarketReqDept";
                    JSONObject param = new JSONObject();
                    currentPage += 1;

                    try {
                        param.put("UNIVNO", OnbridPreference.getString(getContext(), "univNo"));
                        param.put("CAMPUSNO", OnbridPreference.getString(getContext(), "campusNo"));
                        param.put("REQDEPTNO", OnbridPreference.getString(getContext(), "deptNo"));
                        param.put("LOGINEMP", OnbridPreference.getString(getContext(), "loginEmp"));
                        param.put("PAGENUM", currentPage);
                        param.put("PAGEROW", pageRow);

                        OnbridRestClient.post(getContext(), url, param, new OnbridResponseHandler(getContext()) {

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                //super.onSuccess(statusCode, headers, response);
                                Log.d("goMarketList", response.toString());
                                // {"listEndRow":0,"listStartRow":-19,"listTotalCount":9,"listCurrentPage":0, "list":[{},{},,,{}]}
                                // TOTALPAGE, TOTALROW, PAGENUM
                                try {

                                    JSONArray jsonArray = response.getJSONArray("list");

                                    totalPage = ((JSONObject) jsonArray.get(0)).getInt("TOTALPAGE");

                                    ArrayList<MarketItem> data = new ArrayList<MarketItem>();

                                    JSONObject item;
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        item = (JSONObject) jsonArray.get(i);
                                        data.add(new MarketItem(item));
                                    }
                                    marketListAdaptor.addAll(data);
                                    marketListAdaptor.notifyDataSetChanged();


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onFinish() {
                                loading = false;
                                super.onFinish();
                            }
                        });
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        });

    }

    /**
     * 컨트롤 초기화 한다.
     */
    private void clearControll() {
        currentPage =   0;
        totalPage   =   0;
        loading = false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_fragment_market, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_save :
                // 재고활용 상품 등록
                Intent intent = new Intent(getActivity(), MarketPlusActivity.class);
                startActivity(intent);
                break;
            default : break;
        }



        return super.onOptionsItemSelected(item);
    }



    private void goMarketList() {
        String url = "/ws/am/marketAsset/selectMarketReqDept";
        JSONObject param = new JSONObject();

        currentPage += 1;

        try {
            param.put("UNIVNO"      , OnbridPreference.getString(getContext(), "univNo"));
            param.put("CAMPUSNO"    , OnbridPreference.getString(getContext(), "campusNo"));
            param.put("REQDEPTNO"   , OnbridPreference.getString(getContext(), "deptNo"));
            param.put("LOGINEMP"    , OnbridPreference.getString(getContext(), "loginEmp"));
            param.put("PAGENUM"     , currentPage);
            param.put("PAGEROW"     , pageRow);

            OnbridRestClient.post(getContext(), url, param, new OnbridResponseHandler(getContext()) {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    //super.onSuccess(statusCode, headers, response);
                    Log.d("goMarketList", response.toString());
                    // {"listEndRow":0,"listStartRow":-19,"listTotalCount":9,"listCurrentPage":0, "list":[{},{},,,{}]}
                    // TOTALPAGE, TOTALROW, PAGENUM
                    try {
                        if (response.getInt("listTotalCount") < 1) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setMessage("신청된 재고활용 내용이 없습니다.");
                            builder.setTitle("검색결과");
                            builder.setPositiveButton("닫기", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    clearControll();
                                }
                            });
                            builder.show();
                            return;
                        }

                        JSONArray jsonArray = response.getJSONArray("list");

                        totalPage = ((JSONObject) jsonArray.get(0)).getInt("TOTALPAGE");

                        marketItems = new ArrayList<MarketItem>();

                        JSONObject item;
                        for (int i=0; i<jsonArray.length(); i++) {
                            item = (JSONObject) jsonArray.get(i);
                            marketItems.add(new MarketItem(item));
                        }

                        tvNoData.setVisibility(View.GONE);
                        underLine.setVisibility(View.VISIBLE);

                        initViewControll();



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private void deleteMarketAsset(MarketItem item, final int position) {

        String url = "/ws/am/marketAsset/deleteMarketAsset";
        JSONObject param = new JSONObject();

        try {
            param.put("UNIVNO"   , OnbridPreference.getString(getContext(), "univNo"));
            param.put("CAMPUSNO" , OnbridPreference.getString(getContext(), "campusNo"));
            param.put("WORKNO"   , item.getWorkNo());
            param.put("LOGINEMP" , OnbridPreference.getString(getContext(), "loginEmp"));

            OnbridRestClient.post(getContext(), url, param, new OnbridResponseHandler(getContext()) {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    //super.onSuccess(statusCode, headers, response);
                    // Log.d("goMarketList", response.toString());

                    marketItems.remove(position);

                    marketListAdaptor.notifyDataSetChanged();

                }

            });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


} //  class
