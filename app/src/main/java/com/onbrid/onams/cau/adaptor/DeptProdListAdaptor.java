package com.onbrid.onams.cau.adaptor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.onbrid.onams.cau.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Enable on 2016-01-18.
 */
public class DeptProdListAdaptor extends ArrayAdapter {

    public DeptProdListAdaptor(Context context, ArrayList<JSONObject> assetList) {
        super(context, 0, assetList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // return super.getView(position, convertView, parent);

        final JSONObject item = (JSONObject) getItem(position);
        ViewHoler viewHoler;

        if (convertView == null) {
            viewHoler = new ViewHoler();
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_deptprodlist, parent, false);
//
            viewHoler.prodName = (TextView) convertView.findViewById(R.id.prodName);
            viewHoler.cnt      = (TextView) convertView.findViewById(R.id.prodCnt);

            convertView.setTag(viewHoler);


        } else {
            viewHoler = (ViewHoler) convertView.getTag();
        }

        try {
            viewHoler.prodName.setText(item.getString("PRODNAME"));
            viewHoler.cnt     .setText(item.getString("CNT"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return convertView;
    }

    private static class ViewHoler {
        TextView prodName;
        TextView cnt;
    }
}
