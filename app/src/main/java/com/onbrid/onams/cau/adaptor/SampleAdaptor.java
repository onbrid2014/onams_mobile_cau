package com.onbrid.onams.cau.adaptor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.onbrid.onams.cau.aa.SwipeListView;
import com.onbrid.onams.cau.model.Code;
import com.onbrid.onams.cau.R;

import java.util.ArrayList;

//import com.fortysevendeg.swipelistview.SwipeListView;

/**
 * Created by wonzopein on 15. 3. 20..
 */
public class SampleAdaptor extends ArrayAdapter {


    public SampleAdaptor(Context context, ArrayList objects) {
        super(context, 0, objects);
    }


    private static class ViewHolder {
        TextView itemNo;
        TextView itemName;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Code code   =   (Code) getItem(position);
        ViewHolder viewHolder;

        if( convertView == null ) {
            viewHolder = new ViewHolder();

            convertView =   LayoutInflater.from(getContext()).inflate(R.layout.list_layout_sample, parent, false);

            viewHolder.itemName =   (TextView) convertView.findViewById(R.id.example_row_tv_title);
            viewHolder.itemNo =   (TextView) convertView.findViewById(R.id.example_row_tv_description);

            convertView.setTag(viewHolder);
        }else{
            viewHolder  =   (ViewHolder) convertView.getTag();
        }

        ((SwipeListView)parent).recycle(convertView, position);

        viewHolder.itemNo.setText( code.getNo() );
        viewHolder.itemName.setText( code.getName() );

        return convertView;
    }
}
