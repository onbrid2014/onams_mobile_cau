package com.onbrid.onams.cau.adaptor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.onbrid.onams.cau.model.Code;
import com.onbrid.onams.cau.R;

import java.util.ArrayList;

/**
 * Created by Enable on 2015-10-13.
 */
public class CodeListAdaptor extends ArrayAdapter {

    private int selectLayout = -1;

    public CodeListAdaptor(Context context, ArrayList objects) {
        super(context, 0, objects);
    }

    public CodeListAdaptor(Context context, ArrayList objects, int selectLayout) {
        super(context, 0, objects);
        this.selectLayout = selectLayout;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // return super.getView(position, convertView, parent);

        Code code = (Code) getItem(position);
        ViewHoler viewHoler;

        if (convertView == null) {
            viewHoler = new ViewHoler();

            if (this.selectLayout != -1) {
                convertView = LayoutInflater.from(getContext()).inflate(this.selectLayout, parent, false);
            } else {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_layout_code, parent, false);
            }

            viewHoler.itemName  = (TextView) convertView.findViewById(R.id.itemName);
            viewHoler.itemNo    = (TextView) convertView.findViewById(R.id.itemNo);

            convertView.setTag(viewHoler);

        } else {
            viewHoler = (ViewHoler) convertView.getTag();
        }

        viewHoler.itemNo.setText(code.getNo());
        viewHoler.itemName.setText(code.getName());

        return convertView;
    }


    private static class ViewHoler {
        TextView itemNo;
        TextView itemName;
    }
}
