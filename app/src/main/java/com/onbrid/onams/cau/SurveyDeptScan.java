package com.onbrid.onams.cau;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.onbrid.onams.cau.adaptor.CodeDeleteAdaptor;
import com.onbrid.onams.cau.barcodescan.IntentIntegrator;
import com.onbrid.onams.cau.barcodescan.IntentResult;
import com.onbrid.onams.cau.model.SrvyDptScnItem;
import com.onbrid.onams.cau.usb.UsbService;
import com.onbrid.onams.cau.util.OnbridPreference;
import com.onbrid.onams.cau.util.OnbridResponseHandler;
import com.onbrid.onams.cau.util.OnbridRestClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Set;

import cz.msebera.android.httpclient.Header;

public class SurveyDeptScan extends AppCompatActivity {

    private Toolbar toolbar;

    private CodeDeleteAdaptor codeDeleteAdaptor;
    private ArrayList<SrvyDptScnItem> surveyList;

    private UsbSerialHandler usbSerialHandler;
    private UsbService usbService;

    private EditText    surveyNo;
    private TextView    scanCnt;
    private TextView    targetCnt;
    private TextView    noCnt;
    private ListView    scanList;

    private int         readCnt;

    /**
     * 온클릭 리스너
     */
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnScan : // Log.d("onClickListener", "btnScan");
                    IntentIntegrator intentIntegrator = new IntentIntegrator();
                    intentIntegrator.initiateScan(SurveyDeptScan.this);
                    break;
                case R.id.searchSummary : Log.d("onClickListener", "searchSummary");

                    Intent intent = new Intent(getApplication(), SurveyDeptSummary.class);
                    intent.putExtras(getIntent().getExtras());
                    startActivity(intent);
                    break;
                case R.id.btnSave :
                    insertSurveyAsset();
                    break;
                case R.id.btnClose :
                    finish();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_dept_scan);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        setControll();
        if (OnbridPreference.getString(this, "useScanner").equalsIgnoreCase("2") ) {
            usbSerialHandler = new UsbSerialHandler(this);
        }

        getSummaryCount();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (OnbridPreference.getString(this, "useScanner").equalsIgnoreCase("2") ) {
            setFilters();  // Start listening notifications from UsbService
            startService(UsbService.class, usbConnection, null); // Start UsbService(if it was not started before) and Bind it
        }
    }

    @Override
    protected void onPause() {
        if (OnbridPreference.getString(this, "useScanner").equalsIgnoreCase("2") ) {
            try {
                unregisterReceiver(mUsbReceiver);
                unbindService(usbConnection);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (OnbridPreference.getString(this, "useScanner").equalsIgnoreCase("2") ) {
            try {
                unregisterReceiver(mUsbReceiver);
                unbindService(usbConnection);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        super.onDestroy();
    }

    /**
     * 조사된 재물번호를 삭제한다
     * @param position
     */
    public void deleteSurveyNo(int position) {
        Log.d("deleteSurveyNo", "" + position);
        surveyList.remove(position);
        readCnt -= 1;
        setText(scanCnt, readCnt);
        codeDeleteAdaptor.notifyDataSetChanged();
    }

    /**
     * 스캔/키인한 재물번호를 추가한다.
     */
    public void addSurveyNo() {
        // surveyList.add(surveyNo.getText().toString().trim());
        for (int i=0; i<surveyList.size(); i++) {
            if (surveyList.get(i).getBARCODE().equalsIgnoreCase(surveyNo.getText().toString().trim())) {
                Toast.makeText(this, "이미 "+surveyNo.getText().toString().trim()+"는 수집되었습니다.", Toast.LENGTH_SHORT).show();
                setText(surveyNo, "");
                return;
            }
        }

        SrvyDptScnItem scanItem = new SrvyDptScnItem(this);
        scanItem.setBARCODE(surveyNo.getText().toString().trim());
        scanItem.setPLANNO(getIntent().getStringExtra("PLANNO"));
        scanItem.setSCANDEPTNO(getIntent().getStringExtra("DEPTNO"));
        scanItem.setSCANPLACENO(getIntent().getStringExtra("PLACENO"));

        // SURVEYGUBN IS '재물조사 방식 구분 ( 0:장소기반, 1:부서기반, 2:읽은기반)'
        scanItem.setSURVEYGUBN(getIntent().getStringExtra("PLACENO").equalsIgnoreCase("")?1:0);

        surveyList.add(0, scanItem);

        setText(surveyNo, "");
        readCnt += 1;
        setText(scanCnt, readCnt);
        codeDeleteAdaptor.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_survey_dept_scan, menu);
        return true;
    }

    /**
     * 화면 상단의 메뉴 전송/닫기의 이벤트 처리이다
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Log.e("menuSelected", ""+item.getItemId());
        switch (item.getItemId()) {
            case R.id.action_save : // Log.e("menuSelected", "action_save");
                insertSurveyAsset();
                break;

            default : // Log.e("menuSelected", "default");
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void insertSurveyAsset() {
        if (readCnt < 1) {
            Toast.makeText(this, "조사한 내용이 없습니다.", Toast.LENGTH_LONG).show();
            return;
        }

        String url = "/ws/pda/asset/insert";
        JSONObject param = new JSONObject();

        String PLANNO      = getIntent().getStringExtra("PLANNO");
        String SCANPLACENO = getIntent().getStringExtra("PLACENO");
        String SCANDEPTNO  = getIntent().getStringExtra("DEPTNO");

        try {
            param.put("UNIVNO"  , OnbridPreference.getString(this, "univNo"));
            param.put("CAMPUSNO", OnbridPreference.getString(this, "campusNo"));
            param.put("PDANO"   , OnbridPreference.getString(this, "loginEmp"));
            param.put("WORKTYPE", 0);

            JSONArray paramArr = new JSONArray();
            for (int i=0; i<readCnt; i++) {
                paramArr.put(surveyList.get(i).getScanItem());
            }
            param.put("DATABLOCK", paramArr);
            // Log.d("param", param.toString());

            OnbridRestClient.post(this, url, param, new OnbridResponseHandler(this) {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Log.d("onSuccess", response.toString());

                    readCnt = 0;
                    setText(surveyNo, "");
                    setText(scanCnt     , readCnt);
                    surveyList.clear();
                    codeDeleteAdaptor.notifyDataSetChanged();

                    actionSaveCallBack();

                }
            });


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * 호출된 인텐트의 결과값을 받는다
     * 스캔 라이브러리 사용 zxing
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {

            if(result.getContents() == null) {
                Log.d("MainActivity", "Cancelled scan");
                Toast.makeText(this, "재물번호 바코드 스캔 취소됨", Toast.LENGTH_SHORT).show();
            } else {
                Log.d("MainActivity", "Scanned");
                // String scanCode = result.getContents().trim();
                setText(surveyNo, result.getContents().trim());
                addSurveyNo();
            }

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }*/

        switch(requestCode) {
            case IntentIntegrator.REQUEST_CODE:
            {
                if (resultCode == RESULT_CANCELED){
                    Toast.makeText(this, "재물번호 바코드 스캔 취소됨", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

                    String UPCScanned = scanResult.getContents();

                    setText(surveyNo, UPCScanned.trim());
                    addSurveyNo();
                }
                break;
            }
        }


    }

    /**
     * 컨트롤 연결한다
     */
    private void setControll() {
        surveyNo    = (EditText) findViewById(R.id.etSurveyNo);
        scanCnt     = (TextView) findViewById(R.id.tvScanCnt);
        targetCnt   = (TextView) findViewById(R.id.tvTargetCnt);
        noCnt       = (TextView) findViewById(R.id.tvNoCnt);
        scanList    = (ListView) findViewById(R.id.scanList);
        ((TextView) findViewById(R.id.tvScanPlaceName)).setText(getIntent().getStringExtra("PLACENAME"));
        ((TextView) findViewById(R.id.tvScanDeptName)) .setText(getIntent().getStringExtra("DEPTNAME"));
        findViewById(R.id.searchSummary).setOnClickListener(onClickListener);
        findViewById(R.id.btnSave).setOnClickListener(onClickListener);
        findViewById(R.id.btnScan).setOnClickListener(onClickListener);
        findViewById(R.id.btnClose).setOnClickListener(onClickListener);
        // findViewById(R.id.btnSearch).setOnClickListener(onClickListener);

        surveyNo.setOnKeyListener(new EditMessageOnKeyListener());

        // 권한에 따라 키인조사를 설정한다.
        if( !OnbridPreference.getString(this, "adminGubn").equalsIgnoreCase("1") ) {
            surveyNo.setEnabled(false);
            surveyNo.setHint("재물번호를 스캔하세요.");
        }
        // 설정에 따라 재물번호 스캔/조회 버튼 활성화 처리
        if ( Integer.parseInt(OnbridPreference.getString(this, "useScanner")) > 0 ) {
            findViewById(R.id.btnScan).setVisibility(View.GONE);
            //findViewById(R.id.btnSearch).setVisibility(View.GONE);
        }

        initControll();
    }

    /**
     * 컨트롤 초기화한다.
     */
    private void initControll() {
        readCnt = 0;
        setText(surveyNo    , "");
        setText(scanCnt     , readCnt);
        setText(targetCnt   , 0);
        setText(noCnt       , 0);

        initListView();
    }

    /**
     * 리스트뷰를 초기화한다.
     */
    private void initListView() {
        surveyList = new ArrayList<>();
        codeDeleteAdaptor = new CodeDeleteAdaptor(this, surveyList);

        scanList.setAdapter(codeDeleteAdaptor);

    }

    /**
     * EditText, TextView 에 text(내용)을 셋한다.
     * @param item
     * @param val
     */
    private void setText(TextView item, Object val) {
        item.setText(val.toString());
    }


    /**
     * 현재 조사중인 곳의 대상수 등 집계를 가져온다
     */
    private void getSummaryCount() {

        String url = "/ws/pda/summary/dept";
        JSONObject param = new JSONObject();

        try {
            Intent intent = getIntent();
            param.put("UNIVNO", OnbridPreference.getString(this, "univNo"));
            param.put("CAMPUSNO", OnbridPreference.getString(this, "campusNo"));
            param.put("PLANNO", intent.getStringExtra("PLANNO"));
            param.put("DEPTNO", intent.getSerializableExtra("DEPTNO"));
            if (intent.getStringExtra("PLACENO") != null && !intent.getStringExtra("PLACENO").equalsIgnoreCase("")) {
                param.put("PLACENO", intent.getStringExtra("PLACENO"));
            }
            // Log.d("param", param.toString());

            OnbridRestClient.post(this, url, param, new OnbridResponseHandler(this) {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Log.d("onSuccess", response.toString());
                    try {
                        setText(targetCnt, response.getString("C_CNT"));
                        setText(noCnt, response.getString("T_CNT"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void actionSaveCallBack() {
        Toast.makeText(this, "전송되었습니다.", Toast.LENGTH_SHORT).show();
        getSummaryCount();

    }


    /**
     * UsbSerial 스캔 이벤트 결과 처리
     * @param data
     */
    private void getHandleMessage( String data ) {
        setText(surveyNo, data.trim());
        addSurveyNo();
    }

    /**
     * Inner Class UsbSerialHandler
     */
    private static class UsbSerialHandler extends Handler {
        private final WeakReference<SurveyDeptScan> mActivity;

        public UsbSerialHandler(SurveyDeptScan activity) { mActivity = new WeakReference<SurveyDeptScan>(activity); }

        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case UsbService.MESSAGE_FROM_SERIAL_PORT:
                    String data = ((String) msg.obj).trim();
                    if (data.length() > 0) {
                        // Log.e("USB_DATA", data + "/" + data.length());
                        mActivity.get().getHandleMessage(data);
                    }
                    break;
            }
        }
    }

    private void startService(Class<?> service, ServiceConnection serviceConnection, Bundle extras)
    {
        if(UsbService.SERVICE_CONNECTED == false)
        {
            Intent startService = new Intent(this, service);
            if(extras != null && !extras.isEmpty())
            {
                Set<String> keys = extras.keySet();
                for(String key: keys)
                {
                    String extra = extras.getString(key);
                    startService.putExtra(key, extra);
                }
            }
            startService(startService);
        }
        Intent bindingIntent = new Intent(this, service);
        bindService(bindingIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    private void setFilters()
    {
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbService.ACTION_USB_PERMISSION_GRANTED);
        filter.addAction(UsbService.ACTION_NO_USB);
        filter.addAction(UsbService.ACTION_USB_DISCONNECTED);
        filter.addAction(UsbService.ACTION_USB_NOT_SUPPORTED);
        filter.addAction(UsbService.ACTION_USB_PERMISSION_NOT_GRANTED);
        registerReceiver(mUsbReceiver, filter);
    }

    /*
	 * Notifications from UsbService will be received here.
	 */
    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context arg0, Intent arg1)
        {
            if(arg1.getAction().equals(UsbService.ACTION_USB_PERMISSION_GRANTED)) // USB PERMISSION GRANTED
            {
                Toast.makeText(arg0, "USB 시리얼 스캐너 준비됨", Toast.LENGTH_SHORT).show();
            }else if(arg1.getAction().equals(UsbService.ACTION_USB_PERMISSION_NOT_GRANTED)) // USB PERMISSION NOT GRANTED
            {
                Toast.makeText(arg0, "USB 통신 권한없음", Toast.LENGTH_SHORT).show();
            }else if(arg1.getAction().equals(UsbService.ACTION_NO_USB)) // NO USB CONNECTED
            {
                Toast.makeText(arg0, "USB 연결안됨", Toast.LENGTH_SHORT).show();
            }else if(arg1.getAction().equals(UsbService.ACTION_USB_DISCONNECTED)) // USB DISCONNECTED
            {
                Toast.makeText(arg0, "USB 분리됨", Toast.LENGTH_SHORT).show();
            }else if(arg1.getAction().equals(UsbService.ACTION_USB_NOT_SUPPORTED)) // USB NOT SUPPORTED
            {
                Toast.makeText(arg0, "USB 장치 지원안함", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private final ServiceConnection usbConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName arg0, IBinder arg1)
        {
            usbService = ((UsbService.UsbBinder) arg1).getService();
            usbService.setHandler(usbSerialHandler);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0)
        {
            usbService = null;
        }
    };


    /**
     * 조사번호 입력시 엔터 키인을 잡아낸다
     */
    private class EditMessageOnKeyListener implements View.OnKeyListener {

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {

            switch (event.getAction()) {
                case KeyEvent.ACTION_UP:
                    // Log.d("KeyAction", "KeyEvent.ACTION_UP");
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        Log.d("KeyAction", "KeyEvent.KEYCODE_ENTER");
                        addSurveyNo();
                    }
                    break;
            }

            return false;
        }
    }

} // class
