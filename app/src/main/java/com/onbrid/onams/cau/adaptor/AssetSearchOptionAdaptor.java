package com.onbrid.onams.cau.adaptor;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.onbrid.onams.cau.model.Option;
import com.onbrid.onams.cau.R;

import java.util.ArrayList;

/**
 * Created by Enable on 2015-10-13.
 */
public class AssetSearchOptionAdaptor extends ArrayAdapter {

    public AssetSearchOptionAdaptor(Context context, ArrayList<Option> codeList) {
        super(context, 0, codeList);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // return super.getView(position, convertView, parent);

        final Option item = (Option) getItem(position);
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();

            convertView = LayoutInflater.from( getContext() ).inflate(R.layout.list_layout_search_option, parent, false);

            /* UI 엘리먼트를 매핑 */
            viewHolder.codeTypeName     = (TextView) convertView.findViewById(R.id.codeTypeName);
            viewHolder.codeNo           = (TextView) convertView.findViewById(R.id.codeNo);
            viewHolder.codeName         = (TextView) convertView.findViewById(R.id.codeName);
            viewHolder.codeTypeSymbol   = (ImageView) convertView.findViewById(R.id.codeTypeSymbol);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        Log.e( "item.Name", item.getType() );


        /* UI 엘리먼트에 값을 바인딩 */
        if ( item.getType() != null ) {

            Log.e( "item.getState", item.getState()+"" );

            /* 선택된 항목이 있을 때 */
            if (item.getState() != -1) {

                viewHolder.codeNo.setText( item.getNo() );
                viewHolder.codeName.setText( item.getName() );

            } else {

                if (item.isRequired()) {
                    viewHolder.codeNo.setText("");
                    viewHolder.codeName.setText("이 항목은 필수 항목입니다.");
                } else {
                    viewHolder.codeNo.setText("");
                    viewHolder.codeName.setText("-");
                }

            }

        } else {
            viewHolder.codeNo.setText(item.getNo());
            viewHolder.codeName.setText(item.getName());
        }

        item.setIndex(position);
        viewHolder.codeTypeName.setText(item.getTypeName());
        viewHolder.codeTypeSymbol.setImageResource(item.getDraw());

        return convertView;
    }

    private static class ViewHolder {
        TextView codeTypeName;
        TextView codeNo;
        TextView codeName;

        ImageView codeTypeSymbol;
    }
}
