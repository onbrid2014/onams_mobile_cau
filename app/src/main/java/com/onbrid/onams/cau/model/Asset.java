package com.onbrid.onams.cau.model;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Enable on 2015-10-30.
 */
public class Asset implements Serializable {

    private String id;
    private String name;

    private String deptNo;
    private String deptName;

    private String placeNo;
    private String placeName;



    public Asset(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Asset(JSONObject object){

        try{

            setId(object.get("ASSETNO").toString());
            setName(object.get("ASSETNAME").toString());
            setDeptNo(object.get("DEPTNO").toString());
            setDeptName(object.get("DEPTNAME").toString());
            setPlaceNo(object.get("PLACENO").toString());
            setPlaceName(object.get("PLACENAME").toString());

        }catch (Exception e){

            System.out.println("Asset from JSONOBJECT : 실패!");

        }
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeptNo() {
        return deptNo;
    }

    public void setDeptNo(String deptNo) {
        this.deptNo = deptNo;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getPlaceNo() {
        return placeNo;
    }

    public void setPlaceNo(String placeNo) {
        this.placeNo = placeNo;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

}
