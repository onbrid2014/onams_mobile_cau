package com.onbrid.onams.cau.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.onbrid.onams.cau.AssetListActivity;
import com.onbrid.onams.cau.SearchCode;
import com.onbrid.onams.cau.model.Code;
import com.onbrid.onams.cau.model.Option;
import com.onbrid.onams.cau.util.OnbridPreference;
import com.onbrid.onams.cau.R;
import com.onbrid.onams.cau.adaptor.AssetSearchOptionAdaptor;
import com.onbrid.onams.cau.adaptor.CodeListAdaptor;
import com.onbrid.onams.cau.dialog.OnbridProgressDialog;
import com.onbrid.onams.cau.model.AssetScanItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;

/**
 * 부서재물조사 메인 화면
 */
public class SurveyAsset extends Fragment {

    ListView listviewOption;
    AssetSearchOptionAdaptor assetSearchOptionAdaptor;



    Map searchOption;

    View rootView;

    Menu menu;

    Toast toast;

    // 리스트뷰 롱클릭 이벤트 포지션
    private int selectedPos = -1;


    private final static String OPTION_PLAN     =   "PLAN";
    private final static String OPTION_PLACE    =   "PLACE";
    private final static String OPTION_DEPT     =   "DEPT";
    private final static String OPTION_SCAN_YN  =   "SCAN_YN";



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_survey_asset, container, false);

        /* enable onCreateOptionsMenu */
        setHasOptionsMenu(true);

        ((TextView) rootView.findViewById(R.id.tvTitle)).setText("부서 재물조사(체크방식)");


        this.initListviewOption();

        // System.out.println("Flagment2 onCreateView");

        return rootView;
    }

//    @Override
//    public boolean onMenuItemClick(MenuItem item) {
//
//
//        if( item.getItemId() == R.id.action_search ){
//
//            System.out.println( item.getItemId() +" > 조회!");
//            new DataManager().execute();
//
//        }else if( item.getItemId() == R.id.action_clear ){
//
//            System.out.println( item.getItemId() +" > 초기화!");
//
//            int size = assetSearchOptionAdaptor.getCount();
//            for( int i=0; i<size ; i++ ){
//                ((Option) assetSearchOptionAdaptor.getItem(i)).clear();
//            }
//
//            this.initParam();
//            assetSearchOptionAdaptor.notifyDataSetChanged();
//
//        }
//
//        return false;
//    }

    @Override
    public void onStart() {
        super.onStart();

        System.out.println("Flagment onStart");
    }


    @Override
    public void onPause() {
        super.onPause();
        System.out.println("Flagment onPause");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        System.out.println("Flagment onDestroy");
    }


    private void initParam(){
        /* 재물조회 파라미터 초기화 */
        if(searchOption==null){
            searchOption = new HashMap();
        }
        searchOption.put("UNIVNO"    , OnbridPreference.getString(getActivity(), "univNo"));
        searchOption.put("CAMPUSNO"  , OnbridPreference.getString(getActivity(), "campusNo"));
        searchOption.put("PLANNO", "");
        searchOption.put("PAGENUM", 1);
        searchOption.put("PAGEROW", 20);
        searchOption.put("LOGINEMP", "1001");

        searchOption.put("PLACENO", "");
        searchOption.put("PLACENAME", "");
        searchOption.put("DEPTNO", "");
        searchOption.put("DEPTNAME", "");
        searchOption.put("PLANNO", "");
        searchOption.put("PLANSTATUS", "");
    }

    private void initListviewOption(){


        this.initParam();

        /* 조회조건 추가 */
        ArrayList<Option> options = new ArrayList<>();
        options.add(new Option("0000", "조사명", OPTION_PLAN, R.drawable.ic_action_labels));
        options.add(new Option("0001", "부서정보", OPTION_DEPT, R.drawable.ic_action_group));
        options.add(new Option("0002", "위치정보", OPTION_PLACE, R.drawable.ic_action_place));
        options.add(new Option("0003", "조사상태", OPTION_SCAN_YN, R.drawable.ic_action_accept_light));

        options.get(0).setRequired(true);
        options.get(1).setRequired(true);

        /* 검색조건 리스트뷰/아탑터 초기화 */
        listviewOption              =   (ListView) rootView.findViewById(R.id.listviewOption);
        assetSearchOptionAdaptor    =   new AssetSearchOptionAdaptor( getActivity(), options);
        listviewOption.setAdapter(assetSearchOptionAdaptor);


        /* 검색조건 리스트 클릭 이벤트 */
        listviewOption.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Option option = (Option) listviewOption.getItemAtPosition(position);

                //  조사상태 콜백
                if (OPTION_SCAN_YN.equalsIgnoreCase(option.getType())) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("조사상태 선택");

                    ArrayList<Code> arrayList = new ArrayList<Code>();
                    arrayList.add(new Code("0", "전체"));
                    arrayList.add(new Code("2", "있음"));
                    arrayList.add(new Code("3", "없음"));
                    arrayList.add(new Code("4", "미확인"));

                    final CodeListAdaptor arrayAdapter = new CodeListAdaptor(getActivity(), arrayList, R.layout.list_layout_code_simple);

                    builder.setNegativeButton("취소", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    builder.setSingleChoiceItems(arrayAdapter, -1, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            onDialogResult(which, (Code) arrayAdapter.getItem(which));
                            dialog.dismiss();
                        }
                    });
                    builder.create().show();


                    // 조사상태 외 콜백
                } else {

                    Intent intent = new Intent(getActivity(), SearchCode.class);
                    intent.putExtra("position", position);
                    intent.putExtra("type", option.getType());
                    startActivityForResult(intent, 1);

                }

            }


        });

        /* 검색조건 리스트 롱!! 클릭 이벤트 ## 검색 조건을 초기화한다. */
        listviewOption.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                // 조사항목(셀렉트BOX)는 롱클릭 하지 않는다.
                if (position > 2) {
                    return false;
                }

                String title = "";

                selectedPos = position;

                switch (selectedPos) {
                    case 0 : title = "조사계획"; break;
                    case 1 : title = "부서"; break;
                    case 2 : title = "장소"; break;
                }

                AlertDialog.Builder alertDlg = new AlertDialog.Builder(view.getContext());
                alertDlg.setTitle(title + "조건 초기화");

                alertDlg.setPositiveButton("예", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        ((Option) assetSearchOptionAdaptor.getItem(selectedPos)).clear();
                        assetSearchOptionAdaptor.notifyDataSetChanged();

                        switch (selectedPos) {
                            case 0 : searchOption.put("PLANNO", ""); break;
                            case 1 :
                                searchOption.put("DEPTNO", "");
                                searchOption.put("DEPTNAME", "");
                                break;
                            case 2 :
                                searchOption.put("PLACENO", "");
                                searchOption.put("PLACENAME", "");
                                break;
                        }

                        dialog.dismiss();
                    }
                });

                alertDlg.setNegativeButton("아니오", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alertDlg.show();

                return false;
            }
        });

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_fragment_main2, menu);

//        ((MenuItem) menu.getItem(0)).setOnMenuItemClickListener(this);
//        ((MenuItem) menu.getItem(1)).setOnMenuItemClickListener(this);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        System.out.println("onOptionsItemSelected : " + item.getItemId());

        switch ( item.getItemId() ){

            //  검색버 튼
            case    R.id.action_search :

                /*
                    TODO : Validate 처리하기...
                    1. 조사명, 부서 필수 조건으로..
                 */
                if ( searchOption.get("PLANNO").toString().equalsIgnoreCase("") ||
                        searchOption.get("DEPTNO").toString().equalsIgnoreCase("") ) {
                    Toast.makeText(getActivity(), "필수항목을 입력하세요.", Toast.LENGTH_SHORT).show();
                    break;
                }
                ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(getContext().CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

                if (networkInfo != null && networkInfo.isConnected()) {

                    try {
                        Log.e("searchOption", searchOption.toString());
                        String url = OnbridPreference.getString(getActivity(), "url") + "/ws/sa/surveyDept/selectSurveyList";

                        String _message = new Gson().toJson(searchOption);
                        ByteArrayEntity entity = new ByteArrayEntity(_message.getBytes("UTF-8"));

                        final ArrayList<AssetScanItem> data = new ArrayList<AssetScanItem>();

                        AsyncHttpClient client = new AsyncHttpClient();
                        client.post(getActivity(), url, entity, "application/json", new JsonHttpResponseHandler() {

                            @Override
                            public void onStart() {
                                super.onStart();
                                showProgress(true);
                            }

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                //super.onSuccess(statusCode, headers, response);
                                try{
                                    JSONArray jsonArray = (JSONArray) response.getJSONArray("list");
                                    System.out.println("jsonArray : " + jsonArray.length());
                                    System.out.println( jsonArray.toString() );

                                    JSONObject item;
                                    for( int i=0 ; i<jsonArray.length() ; i++ ){
                                        item = (JSONObject) jsonArray.get(i);
                                        data.add(new AssetScanItem( item ));
                                    }

                                    moveToAssetList(data);






                                }catch (Exception e){

                                }
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                //super.onFailure(statusCode, headers, responseString, throwable);
                                Toast.makeText(getActivity(), "데이터가 없습니다.", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFinish() {
                                super.onFinish();
                                showProgress(false);
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getActivity(), "네트워크 연결상태를 확인해 주세요.", Toast.LENGTH_LONG).show();
                }



                //new DataManager().execute();

//                ArrayList data = new ArrayList<Asset>();
//                JSONObject items;
//                try {
//                    for( int i=0; i<50 ; i++ ){
//                        items = new JSONObject();
//                        items.put("ASSETNO", "000000_"+i);
//                        items.put("ASSETNAME", "ASSETNAME_"+i);
//                        items.put("DEPTNO", "000000_"+i);
//                        items.put("DEPTNAME", "DEPTNAME_"+i);
//                        items.put("PLACENO", "000000_"+i);
//                        items.put("PLACENAME", "PLACENAME_"+i);
//                        items.put("PAGENUM", 1);
//                        items.put("TOTALPAGE", 1);
//                        items.put("TOTALROW", 50);
//                        data.add( new AssetScanItem(items));
//                    }
//
//                    moveToAssetList( data );
//
//                }catch (Exception e){
//
//                }



                break;


            //  초기화 버튼
            case    R.id.action_clear :

                int size = assetSearchOptionAdaptor.getCount();
                for( int i=0; i<size ; i++ ){
                    ((Option) assetSearchOptionAdaptor.getItem(i)).clear();
                }

                this.initParam();
                assetSearchOptionAdaptor.notifyDataSetChanged();

                toast   =   Toast.makeText(getActivity(), "검색조건이 초기화 되었습니다.", Toast.LENGTH_SHORT);
                toast.show();

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Toast.makeText(getActivity(), "onActivityResult", Toast.LENGTH_SHORT).show();
        Code returnCode = data.hasExtra("code")? (Code) data.getSerializableExtra("code"):null;
        String type     = data.getStringExtra("type");
        int position    = data.getIntExtra("position", -1);

        if( returnCode == null || position == -1 ){
            return;
        }

        ((Option) assetSearchOptionAdaptor.getItem( position )).set( returnCode );

        switch( type ){
            case OPTION_PLAN  :
                searchOption.put("PLANNO",      returnCode.getName() );
                break;
            case OPTION_PLACE :
                searchOption.put("PLACENO",     returnCode.getNo() );
                searchOption.put("PLACENAME",   returnCode.getName() );
                break;
            case OPTION_DEPT  :
                searchOption.put("DEPTNO",      returnCode.getNo() );
                searchOption.put("DEPTNAME",    returnCode.getName() );
                break;
        }

        assetSearchOptionAdaptor.notifyDataSetChanged();

    }


    public void onDialogResult( int position, Code item ){
        // Toast.makeText(getActivity(), "onDialogResult", Toast.LENGTH_SHORT).show();
        // System.out.println( position +" : " + item.getNo() + "/" + item.getName() );

//        ((Option) assetSearchOptionAdaptor.getItem(3)).setName( item.getName() );
//        ((Option) assetSearchOptionAdaptor.getItem(3)).setNo( item.getNo() );
//
//        assetSearchOptionAdaptor.notifyDataSetChanged();
//        searchOption.put("STATUS", item.getNo() );

        this.setDialogResult( item.getNo(), item.getName() );
    }

    public void setDialogResult( String no, String name ){
        ((Option) assetSearchOptionAdaptor.getItem(3)).setName( name );
        ((Option) assetSearchOptionAdaptor.getItem(3)).setNo( no );

        assetSearchOptionAdaptor.notifyDataSetChanged();
        searchOption.put("CONFGUBN", no );
    }



    OnbridProgressDialog progressDialog;
    public void showProgress(boolean show ){

        if( show ){

            try{
                progressDialog = new OnbridProgressDialog( getActivity() );
                progressDialog.show();
            }catch(Exception e){}

        }else{

            if( progressDialog != null ){
                progressDialog.dismiss();
            }

        }

    }



    public void moveToAssetList( ArrayList<AssetScanItem> items ){

        if( items.size() == 0 ){
            toast   =   Toast.makeText(getActivity(), "조회 대상이 없습니다.", Toast.LENGTH_SHORT);
            toast.show();
            return;
        }

        Intent intent = new Intent( getActivity(), AssetListActivity.class);

        /* 검색결과 */
        intent.putExtra("items", items);

        /* 검색조건 */
        intent.putExtra("PLANNO",        searchOption.get("PLANNO").toString()   );
        intent.putExtra("LOGINEMP",      searchOption.get("LOGINEMP").toString() );
        intent.putExtra("PLACENO",       searchOption.get("PLACENO").toString()  );
        intent.putExtra("PLACENAME",     searchOption.get("PLACENAME").toString()  );
        intent.putExtra("DEPTNO",        searchOption.get("DEPTNO").toString()   );
        intent.putExtra("DEPTNAME",      searchOption.get("DEPTNAME").toString()   );

        startActivity(intent);
        //startActivityForResult(intent, 1);
    }




//    private class DataManager extends AsyncTask {
//
//        private DataManager() {
//        }
//
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//            showProgress(true);
//
//        }
//
//        @Override
//        protected Object doInBackground(Object[] params) {
//
//
//            AsyncHttpClient client = new SyncHttpClient();
//            ByteArrayEntity entity = null;
//
//
//            String _url  =   "http://demo.onbrid.com/onass/ws/sa/surveyDept/selectSurveyList";
//
//            String _message =   new Gson().toJson( searchOption );//"{\"PLANNO\":\"0001\", \"PAGENUM\":1, \"PAGEROW\":10}";
//
//            System.out.println(_message);
//
//            final ArrayList<Asset> data = new ArrayList<Asset>();
//
//            try{
//
//                entity = new ByteArrayEntity( _message.getBytes("UTF-8") );
//
//                client.post(getActivity(), _url, entity, "application/json", new JsonHttpResponseHandler(){
//                    @Override
//                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
//                        super.onSuccess(statusCode, headers, response);
//
//                        try{
//                            JSONArray jsonArray = (JSONArray) response.getJSONArray("list");
//                            System.out.println("jsonArray : " + jsonArray.length());
//                            System.out.println( jsonArray.toString() );
//
//                            JSONObject item;
//                            for( int i=0 ; i<jsonArray.length() ; i++ ){
//                                item = (JSONObject) jsonArray.get(i);
//                                data.add(new AssetScanItem( item ));
//                            }
//
//                        }catch (Exception e){
//
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
//                        super.onFailure(statusCode, headers, throwable, errorResponse);
//                    }
//
//                    @Override
//                    public void onFinish() {
//                        super.onFinish();
//                    }
//                });
//
//
//            }catch (Exception e){
//
//            }
//
//            return data;
//        }
//
//
//
//        @Override
//        protected void onPostExecute(Object o) {
//            super.onPostExecute(o);
//
//            //System.out.println( ((ArrayList<AssetItem>) o).size() +"") ;
//            showProgress(false);
//            moveToAssetList( (ArrayList<AssetScanItem>) o );
//
//        }
//    }
}
