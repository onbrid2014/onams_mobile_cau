package com.onbrid.onams.cau;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.onbrid.onams.cau.util.OnbridPreference;

public class SettingActivity extends PreferenceActivity {

    PreferenceScreen rootScreen;

    EditTextPreference loginId;
    EditTextPreference passWd;

    Preference btSetting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.setting_toolbar);

        addPreferencesFromResource(R.xml.pref_settings);

        LinearLayout root = (LinearLayout)findViewById(android.R.id.list).getParent().getParent().getParent();
        Toolbar bar = (Toolbar) LayoutInflater.from(this).inflate(R.layout.setting_toolbar, root, false);
        root.addView(bar, 0); // insert at top

        bar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentFinish();
            }
        });

        rootScreen = (PreferenceScreen) findPreference("root");

        this.initPreference();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        intentFinish();
    }

    /**
     * 설정 변경시 업데이트
     */
    private void initPreference() {
        /*
         *  로그인정보
         */
        loginId     = (EditTextPreference) findPreference("userId");
        passWd      = (EditTextPreference) findPreference("userPw");

        btSetting = findPreference("bt_address");
        btSetting.setTitle(PreferenceManager.getDefaultSharedPreferences(btSetting.getContext()).getString(btSetting.getKey(), "세팅 안됨"));
        btSetting.setOnPreferenceClickListener(preferenceClickListener);

        setOnPreferenceChange(findPreference("userId"));
//        setOnPreferenceChange(findPreference("univNo"));
        setOnPreferenceChange(findPreference("url"));
//        setOnPreferenceChange(findPreference("surveyPrefix"));
        setOnPreferenceChange(findPreference("useScanner"));
//        setOnPreferenceChange(findPreference("scannerID"));
//        setOnPreferenceChange(findPreference("usbBaudRate"));
//        setOnPreferenceChange(findPreference("useDatabits"));
//        setOnPreferenceChange(findPreference("useStopbits"));
//        setOnPreferenceChange(findPreference("useParity"));

//        rootScreen.removePreference((PreferenceCategory) findPreference("connInfo"));

        PreferenceCategory scanInfo = (PreferenceCategory) findPreference("scanInfo");
        scanInfo.removePreference(findPreference("surveyPrefix"));
        //scanInfo.removePreference(findPreference("scannerID"));
        scanInfo.removePreference(findPreference("usbBaudRate"));
        scanInfo.removePreference(findPreference("useDatabits"));
        scanInfo.removePreference(findPreference("useStopbits"));
        scanInfo.removePreference(findPreference("useParity"));


    }

    private void setOnPreferenceChange(Preference mPreference) {
        mPreference.setOnPreferenceChangeListener(onPreferenceChangeListener);

        onPreferenceChangeListener.onPreferenceChange(
                mPreference,
                PreferenceManager.getDefaultSharedPreferences(
                        mPreference.getContext()).getString(
                        mPreference.getKey(), ""));
    }


    private Preference.OnPreferenceChangeListener onPreferenceChangeListener = new Preference.OnPreferenceChangeListener() {

        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            String stringValue = newValue.toString();

            if (preference instanceof EditTextPreference) {
                preference.setSummary(stringValue);

            } else if (preference instanceof ListPreference) {

                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);

                preference
                        .setSummary(index >= 0 ? listPreference.getEntries()[index]
                                : null);

            }

            return true;
        }

    };

    private Preference.OnPreferenceClickListener preferenceClickListener = new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(final Preference preference) {
            AlertDialog.Builder adb = new AlertDialog.Builder(SettingActivity.this)
                    .setMessage("블루투스 세팅을 초기화 하시겠습니까?")
                    .setCancelable(false)
                    .setPositiveButton("예", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            preference.setTitle("세팅 안됨");
                            OnbridPreference.setString(SettingActivity.this, "bt_address", "");
                        }
                    })
                    .setNegativeButton("아니오", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

            AlertDialog dialog = adb.create();
            dialog.show();
            return true;
        }
    };

    private void intentFinish() {

        Intent intent = getIntent();
        intent.putExtra("userId",     loginId.getText());
        intent.putExtra("userPw",     passWd.getText());
        setResult(1, intent);

        finish();
    }
}
