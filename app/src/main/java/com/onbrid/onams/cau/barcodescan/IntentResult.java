package com.onbrid.onams.cau.barcodescan;

/*
 * Copyright 2009 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class IntentResult {

    private final String contents;
    private final String formatName;

    IntentResult(String contents, String formatName) {
        this.contents = contents;
        this.formatName = formatName;
    }

    /**
     * @return raw content of barcode
     */
    public String getContents() {
        String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
        return contents.trim().replaceAll(match, "");
    }

    /**
     * @return name of format, like "QR_CODE", "UPC_A". See <code>BarcodeFormat</code> for more format names.
     */
    public String getFormatName() {
        return formatName;
    }
}
