package com.onbrid.onams.cau.adaptor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.onbrid.onams.cau.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Enable on 2016-01-18.
 */
public class DeptAssetListAdaptor extends ArrayAdapter {

    public DeptAssetListAdaptor(Context context, ArrayList<JSONObject> assetList) {
        super(context, 0, assetList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //return super.getView(position, convertView, parent);

        final JSONObject item = (JSONObject) getItem(position);
        ViewHoler viewHoler;

        if (convertView == null) {
            viewHoler = new ViewHoler();
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_deptassetlist, parent, false);
//
            viewHoler.assetNo   = (TextView) convertView.findViewById(R.id.assetNo);
            viewHoler.assetName = (TextView) convertView.findViewById(R.id.assetName);
            viewHoler.modelName = (TextView) convertView.findViewById(R.id.modelName);

            convertView.setTag(viewHoler);


        } else {
            viewHoler = (ViewHoler) convertView.getTag();
        }

         try {
             viewHoler.assetNo  .setText(item.getString("ASSETNO"));
             viewHoler.assetName.setText(item.getString("ASSETNAME"));
             viewHoler.modelName.setText(item.getString("MODELNAME"));
         } catch (JSONException e) {
             e.printStackTrace();
         }



        return convertView;
    }

    private static class ViewHoler {

        TextView assetNo;
        TextView assetName;
        TextView modelName;

    }
}
