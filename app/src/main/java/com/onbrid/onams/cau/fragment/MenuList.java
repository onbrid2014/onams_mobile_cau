package com.onbrid.onams.cau.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.onbrid.onams.cau.DrawerActivity;
import com.onbrid.onams.cau.SettingActivity;
import com.onbrid.onams.cau.util.OnbridPreference;
import com.onbrid.onams.cau.R;


public class MenuList extends Fragment implements MenuItem.OnMenuItemClickListener {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // return super.onCreateView(inflater, container, savedInstanceState);

        View rootView = inflater.inflate(R.layout.fragment_menu_list, container, false);

        rootView.findViewById(R.id.btnSearchAsset).setOnClickListener(mLayoutClickListener);
        rootView.findViewById(R.id.btnSurvey).setOnClickListener(mLayoutClickListener);
        rootView.findViewById(R.id.btnSurveyScan).setOnClickListener(mLayoutClickListener);
        rootView.findViewById(R.id.btnSurveyBase).setOnClickListener(mLayoutClickListener);
        // rootView.findViewById(R.id.btnMarket).setOnClickListener(mLayoutClickListener);

        setHasOptionsMenu(true);

        return rootView;
    }


    /**
     * 리스트 클릭 이벤트 생성.
     */
    View.OnClickListener mLayoutClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnSearchAsset :
                    ((DrawerActivity) getActivity()).setFragments(1);
                    break;
                case R.id.btnSurveyScan :
                    ((DrawerActivity) getActivity()).setFragments(2);
                    break;
                case R.id.btnSurvey :
                    ((DrawerActivity) getActivity()).setFragments(3);
                    break;
                case R.id.btnSurveyBase :
                    ((DrawerActivity) getActivity()).setFragments(4);
                    break;
//                case R.id.btnMarket :
//                    ((DrawerActivity) getActivity()).setFragments(5);
//                    break;
            }
        }
    };


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if( item.getItemId() == R.id.action_settings ){
            Intent intent = new Intent(getActivity(), SettingActivity.class);
            startActivity(intent);
        }else{

            OnbridPreference.setBoolean(getActivity(), "isAutoLogin", false);
            getActivity().finish();
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

}
