package com.onbrid.onams.cau;

import android.Manifest;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.onbrid.onams.cau.barcodescan.IntentIntegrator;
import com.onbrid.onams.cau.barcodescan.IntentResult;
import com.onbrid.onams.cau.usb.UsbService;
import com.onbrid.onams.cau.util.ImageUtil;
import com.onbrid.onams.cau.util.OnbridPreference;
import com.onbrid.onams.cau.util.OnbridRestClient;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;

public class SurveyNomalActivity extends ActionBarActivity {

    private Toolbar toolbar;

    // private EditText etProdNm;
    // private EditText etLabelNo;
    private EditText etSurveyNo;
    private EditText etBarcodeNo;
    private EditText etGubunNm;
    private EditText etModelNm;
    private EditText etSpecNm;
    private EditText etSerialNum;

    private Button   btnScan;
    private Button   btnScanAsset;
    private Button   btnSearch;
    private Button   btnSearchAsset;
    private Button   btnGubun;
    private Button   btnAssetInfo;
    private Button   btnAssetPic;
    private Button   btnBT;

    private TextView tvScanPlaceName;
    private TextView tvScanDeptName;
    // private TextView tvAssetInfoCnt;
    // private TextView tvAssetPicCnt;

    private ImageView picImage;

    private int    readingYn = 0;

    // private int    assetInfoCnt;
    // private int    assetPicCnt;

    private int    mDegree = 0;
    private File   pathInfo;
    private File   pathPic;
    private String mPrefix;
    private String mFileName;
    private static final String JPEG_FILE_PATH        = "ONBRID/ASSET";
    private static final String JPEG_FILE_PATH_INFO   = "INFO";
    private static final String JPEG_FILE_PATH_PIC    = "PIC";
    private static final String JPEG_FILE_PREFIX_INFO = "INFO_";
    private static final String JPEG_FILE_PREFIX_PIC  = "PIC_";
    private static final String JPEG_FILE_SUFFIX      = ".jpg";

    private ArrayList<File> mFileList;
    private ArrayList<File> mFilePic;

    private final int REQUEST_ENABLE_BT = 1;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothSocket mmSocket;
    private BluetoothDevice mmDevice;
    private OutputStream mmOutputStream;
    private InputStream mmInputStream;
    private Thread workerThread;
    private byte[] readBuffer;
    private int readBufferPosition;
    private volatile boolean stopWorker;

    private UsbSerialHandler usbSerialHandler;
    private UsbService usbService;

    private int picBtnID = -1;
    private PermissionListener permissionListener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            takePic();
        }

        @Override
        public void onPermissionDenied(ArrayList<String> arrayList) {
            Toast.makeText(SurveyNomalActivity.this, "권한 거부됨.\n" + arrayList.toString(), Toast.LENGTH_LONG).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_nomal);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        initControll();

        if ( OnbridPreference.getString(this, "useScanner").equalsIgnoreCase("1") ) {
            initBluetooth();
        } else if (OnbridPreference.getString(this, "useScanner").equalsIgnoreCase("2") ) {
            usbSerialHandler = new UsbSerialHandler(this);
        }


        String workDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
        pathInfo = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/" + JPEG_FILE_PATH + "/" + JPEG_FILE_PATH_INFO + "/" + workDate);
        if (!pathInfo.exists()) pathInfo.mkdirs();
        pathPic  = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/" + JPEG_FILE_PATH + "/" + JPEG_FILE_PATH_PIC  + "/" + workDate);
        if (!pathPic.exists()) pathPic.mkdirs();

        mFileList = new ArrayList<File>();
        mFilePic = new ArrayList<File>();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("onResume", "onResume");
        if ( OnbridPreference.getString(this, "useScanner").equalsIgnoreCase("1") ) {
            initBluetooth();
        } else if (OnbridPreference.getString(this, "useScanner").equalsIgnoreCase("2") ) {
            setFilters();  // Start listening notifications from UsbService
            startService(UsbService.class, usbConnection, null); // Start UsbService(if it was not started before) and Bind it
        }
    }
    @Override
    public void onPause() {
        super.onPause();
        Log.e("onPause", "onPause");
        try {
            // closeBT();
            unregisterReceiver(mUsbReceiver);
            unbindService(usbConnection);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        try {
            // closeBT();
            unregisterReceiver(mUsbReceiver);
            unbindService(usbConnection);
        } catch (Exception e) { e.printStackTrace(); }

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Log.e("기초조사", "SurveyNomalActivity onBackPressed");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        Log.e("기초조사", "SurveyNomalActivity onCreateOptionsMenu");
        getMenuInflater().inflate(R.menu.menu_survey_nomal, menu);
        return super.onCreateOptionsMenu(menu);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.e("onOptionsItemSelected", item.getItemId() + "");
        switch (item.getItemId()) {
            case R.id.action_save : // 조사 데이터 저장

                if ( etSurveyNo.getText().toString().equalsIgnoreCase("") || etGubunNm.getText().toString().equalsIgnoreCase("") ) {
                    Toast.makeText(this, "조사번호/재물구분을 입력하세요.", Toast.LENGTH_LONG).show();
                    return false;
                }

                if (getNetWorkState()) {
                    try {
                        Intent intent = getIntent();

                        String url = OnbridPreference.getString(SurveyNomalActivity.this, "url") + "/ws/sa/sabase/insertSurveyAssetBase";
                        Map param = new HashMap();
                        param.put("UNIVNO"     , OnbridPreference.getString(SurveyNomalActivity.this, "univNo"));
                        param.put("CAMPUSNO"   , OnbridPreference.getString(SurveyNomalActivity.this, "campusNo"));
                        param.put("SURVEYNO"   , etSurveyNo.getText().toString());
                        param.put("SCANPLACENO", intent.getExtras().getString("SCANPLACENO"));
                        param.put("SCANDEPTNO" , intent.getExtras().getString("SCANDEPTNO"));
                        param.put("SCANEMPNO"  , "");
                        param.put("SCANEMPNAME", "");
                        param.put("BARCODE"    , etBarcodeNo.getText().toString());
                        param.put("READINGYN"  , readingYn);
                        param.put("ASSETNAME"  , "");//etProdNm.getText().toString());
                        param.put("GUBUNNAME"  , etGubunNm.getText().toString());
                        param.put("MODELNAME"  , etModelNm.getText().toString());
                        param.put("SPEC"       , etSpecNm.getText().toString());
                        param.put("SERIALNUM"  , etSerialNum.getText().toString());
                        param.put("WORKER"     , OnbridPreference.getString(SurveyNomalActivity.this, "loginEmp"));
                        param.put("WORKERNAME" , OnbridPreference.getString(SurveyNomalActivity.this, "loginEmpName"));
                        param.put("BIGO"       , "");
                        param.put("LOGINEMP"   , OnbridPreference.getString(SurveyNomalActivity.this, "loginEmp"));

                        String _message = new Gson().toJson(param);
                        // Log.d("action_save", _message);

                        ByteArrayEntity entity = new ByteArrayEntity(_message.getBytes("UTF-8"));

                        AsyncHttpClient client = new AsyncHttpClient();
                        client.post(getParent(), url, entity, "application/json", new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                //super.onSuccess(statusCode, headers, response);
                                Log.e("menu.onSuccess", response.toString());

                                insertFileList();

                                Toast.makeText(getApplicationContext(), "전송 성공!!", Toast.LENGTH_SHORT).show();
                                clearControll(2);

                                // stopWorker = false;
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                //super.onFailure(statusCode, headers, responseString, throwable);
                                Toast.makeText(getApplicationContext(), "정보가 없습니다.", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "네트워크 연결상태를 확인해 주세요.", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.action_clear: // 입력항목 클리어
                clearControll(3);
                // clearImages();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * 버튼 이벤트
     */
    View.OnClickListener btnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnScan  : // zxing 라이브러리를 호출해서 스캔한다.
                    // readingYn = 0;
                    IntentIntegrator intentIntegrator = new IntentIntegrator();
                    intentIntegrator.initiateScan(SurveyNomalActivity.this);
                    break;
                case R.id.btnScanAsset  : // zxing 라이브러리를 호출해서 스캔한다.
                    readingYn = 0;
                    IntentIntegrator intentIntegrator1 = new IntentIntegrator();
                    intentIntegrator1.initiateScan(SurveyNomalActivity.this);
                    break;

                case R.id.btnSearch : // 통신을 해서 조사번호 정보를 가져온다
                    //Log.i("기본조사", "통신을 해서 재물정보를 가져온다.");
                    searchAssetNo(0);
                    break;
                case R.id.btnSearchAsset : // 통신을 해서 재물정보를 가져온다.
                    //Log.i("기본조사", "통신을 해서 재물정보를 가져온다.");
                    searchAssetNo(1);
                    break;
                default : // 이미지 촬영을 한다.
                    Log.i("기본조사", "이미지 촬영을 한다.");
                    picBtnID = v.getId();
                    new TedPermission(SurveyNomalActivity.this)
                            .setPermissionListener(permissionListener)
                            .setDeniedMessage("사용을 거부하면 서비스를 이용할 수 없습니다.\n\n권한 설정해주세요. [환경설정] > [권한]")
                            .setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE)
                            .check();

                    break;
            }
        }
    };



    private void takePic() {
        if (etSurveyNo.getText().toString().length() < 1) {
            Toast.makeText(getApplicationContext(), "기초조사번호를 입력하세요.", Toast.LENGTH_LONG).show();
            return;
        }

        mPrefix = (picBtnID == R.id.btnAssetInfo) ? JPEG_FILE_PREFIX_INFO : JPEG_FILE_PREFIX_PIC;
        String phoneNum = ((TelephonyManager) getApplicationContext().getSystemService(getApplicationContext().TELEPHONY_SERVICE)).getLine1Number();

        mFileName = etSurveyNo.getText().toString() + "_" + mPrefix + phoneNum + "_" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + JPEG_FILE_SUFFIX;

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = null;
        if (picBtnID == R.id.btnAssetInfo) {
            file = new File(pathInfo, mFileName);
        } else {
            file = new File(pathPic , mFileName);
        }

        // if ( !path.exists() ) path.mkdirs();

        Uri uri = Uri.fromFile(file);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);

        startActivityForResult(intent, 0);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case IntentIntegrator.REQUEST_CODE :
                if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(this, "재물번호 바코드 스캔 취소됨", Toast.LENGTH_SHORT).show();
                } else {
                    IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
                    String UPCScanned = scanResult.getContents().trim();

                    if (UPCScanned.indexOf(OnbridPreference.getString(this, "surveyPrefix")) > -1) {
                        etSurveyNo.setText(UPCScanned);
                        searchAssetNo(0);
                    } else {
                        etBarcodeNo.setText(UPCScanned);
                        searchAssetNo(1);
                        readingYn = 1;
                    }

                }
                break;
            case 0 : // 사진촬영 완료
                if (resultCode == RESULT_OK) {
                    Log.i("onActivityResult", "사진촬영 완료!");
                    try {
                        File file = null;
                        if (mPrefix == JPEG_FILE_PREFIX_INFO) {
                            file = new File(pathInfo, mFileName);
                            // assetInfoCnt += 1;
                            // tvAssetInfoCnt.setText(assetInfoCnt+"");

                            mDegree = ImageUtil.GetExifOrientation(file.getAbsolutePath());
                            Bitmap rotateBitmap = ImageUtil.getRotatedBitmap(this, file.getAbsolutePath(), mDegree);
                            // Bitmap roundBitmap = ImageUtil.getRoundedCornerBitmap(rotateBitmap);

                            FileOutputStream outputStream = new FileOutputStream(pathInfo + "/" + mFileName);
                            rotateBitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);
                            outputStream.close();

                            DisplayMetrics metrics = new DisplayMetrics();
                            WindowManager windowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
                            windowManager.getDefaultDisplay().getMetrics(metrics);

                            ImageView iv = new ImageView(this);
                            iv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                            ViewGroup.LayoutParams params = iv.getLayoutParams();
                            params.width = metrics.widthPixels * 1/7;
                            params.height = metrics.widthPixels * 1/7;
                            iv.setLayoutParams(params);

                            iv.setImageBitmap(rotateBitmap);
                            //((LinearLayout)findViewById(R.id.imgLayout)).addView(iv);
                            ((GridLayout)findViewById(R.id.infoGrid)).addView(iv);

                            file = new File(pathInfo, mFileName);
                            mFileList.add(file);

                        } else {
                            file = new File(pathPic, mFileName);
                            // assetPicCnt += 1;
                            // tvAssetPicCnt.setText(assetPicCnt+"");
                            mDegree = ImageUtil.GetExifOrientation(file.getAbsolutePath());
                            Bitmap rotateBitmap = ImageUtil.getRotatedBitmap(this, file.getAbsolutePath(), mDegree);
                            // Bitmap roundBitmap = ImageUtil.getRoundedCornerBitmap(rotateBitmap);

                            FileOutputStream outputStream = new FileOutputStream(pathPic + "/" + mFileName);
                            rotateBitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);
                            outputStream.close();

                            picImage.setImageBitmap(rotateBitmap);

                            mFilePic = new ArrayList<>();
                            file = new File(pathPic, mFileName);
                            mFilePic.add(file);
                        }


                        System.gc();
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        // 재물 사진의 정보를 DB(FileList)에 기록한다
                    }


                } else {
                    Toast.makeText(getApplicationContext(), "사진 촬영에 실패했습니다.", Toast.LENGTH_SHORT).show();
                }
                break;
            case 100 : // scan zxing
                if (resultCode == RESULT_OK) {
                    String contents = data.getStringExtra("SCAN_RESULT");
                    Toast.makeText(getApplicationContext(), contents, Toast.LENGTH_SHORT).show();
                }
                break;
            case REQUEST_ENABLE_BT :
                if (resultCode == RESULT_OK) {
                    viewDeviceListDialog();
                }
                break;
            default: break;
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }



    /**
     * 컨트롤 세팅 이벤트 연결
     */
    private void initControll() {
        // etLabelNo    = (EditText) findViewById(R.id.etLabelNo);
        etBarcodeNo    = (EditText) findViewById(R.id.etAssetNo);
        etSurveyNo   = (EditText) findViewById(R.id.etSurveyNo);
        etGubunNm    = (EditText) findViewById(R.id.etGubunNm);
        // etProdNm     = (EditText) findViewById(R.id.etProdNm);
        etModelNm    = (EditText) findViewById(R.id.etModelNm);
        etSpecNm     = (EditText) findViewById(R.id.etSpecNm);
        etSerialNum  = (EditText) findViewById(R.id.etSerialNum);
        btnScan      = (Button)   findViewById(R.id.btnScan);
        btnScanAsset = (Button)   findViewById(R.id.btnScanAsset);
        btnSearch    = (Button)   findViewById(R.id.btnSearch);
        btnSearchAsset = (Button)   findViewById(R.id.btnSearchAsset);
        btnGubun     = (Button)   findViewById(R.id.btnGubun);
        btnAssetInfo = (Button)   findViewById(R.id.btnAssetInfo);
        btnAssetPic  = (Button)   findViewById(R.id.btnAssetPic);
        btnBT        = (Button)   findViewById(R.id.btnBT);

        tvScanPlaceName = (TextView)   findViewById(R.id.tvScanPlaceName);
        tvScanDeptName  = (TextView)   findViewById(R.id.tvScanDeptName);
        // tvAssetInfoCnt = (TextView)   findViewById(R.id.assetInfoCnt);
        // tvAssetPicCnt  = (TextView)   findViewById(R.id.assetPicCnt);

        picImage     = (ImageView) findViewById(R.id.picImage);

        btnScan.setOnClickListener(btnListener);
        btnScanAsset.setOnClickListener(btnListener);
        btnSearch.setOnClickListener(btnListener);
        btnSearchAsset.setOnClickListener(btnListener);
        btnGubun.setOnClickListener(btnListener);
        btnAssetInfo.setOnClickListener(btnListener);
        btnAssetPic.setOnClickListener(btnListener);
        btnBT.setOnClickListener(btnListener);

        clearControll(1);
        // Log.e("getIntent", intent.getExtras().getString("SCANPLACENANE") + "/" + intent.getExtras().getString("SCANDEPTNANE"));
        tvScanPlaceName.setText(getIntent().getExtras().getString("SCANPLACENAME"));
        tvScanDeptName.setText( (getIntent().getExtras().getString("SCANDEPTNAME").equalsIgnoreCase(""))? "-":getIntent().getExtras().getString("SCANDEPTNAME") );
        // tvAssetInfoCnt.setText(assetInfoCnt + "");
        // tvAssetPicCnt.setText(assetPicCnt+"");

        if ( Integer.parseInt(OnbridPreference.getString(this, "useScanner")) > 0 ) {
            btnScan.setVisibility(View.GONE);
            btnScanAsset.setVisibility(View.GONE);
        }
        disableControll(0);
    }

    /**
     * 입력할목 활성 비활성 조정한다<br>
     * 0: 비활성 , 1: 활성
     * @param gubn
     */
    private void disableControll(int gubn) {

        if (gubn < 1) {
            etBarcodeNo.setEnabled(false);
            etGubunNm.setEnabled(false);
            etModelNm.setEnabled(false);
            etSpecNm.setEnabled(false);
            etSerialNum.setEnabled(false);
        } else {
            etBarcodeNo.setEnabled(true);
            etGubunNm.setEnabled(true);
            etModelNm.setEnabled(true);
            etSpecNm.setEnabled(true);
            etSerialNum.setEnabled(true);
        }
    }

    /**
     * 블루투스 장비와 연결 초기화
     */
    private void initBluetooth() {
//        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
//
//        if (mBluetoothAdapter != null) {
//            // Log.e("initBluetooth", "mBluetoothAdapter != null");
//            if (!mBluetoothAdapter.isEnabled()) {
//                Log.e("initBluetooth", "!mBluetoothAdapter.isEnabled()");
//                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
//
//            } else {
//                viewDeviceListDialog();
//            }
//
//
//
//
//
//        } else {
//            Log.e("initBluetooth", "블루투스 사용할 수 없는 장비일 경우이다.");
//            // 블루투스 사용할 수 없는 장비일 경우이다.
//            // 아무것도 안함.
//        }


    }



    private void viewDeviceListDialog() {
//        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
//        if(pairedDevices.size() > 0)
//        {
//            for(BluetoothDevice device : pairedDevices)
//            {
//                if(device.getName().equals(OnbridPreference.getString(SurveyNomalActivity.this, "scannerID").toUpperCase()))
//                {
//                    mmDevice = device;
//                    break;
//                }
//            }
//        }
//
//        if (mmDevice != null) {
//
//            openBT();
//
//        } else {
//            Toast.makeText(this, "기기(폰)의 설정에서 블루투스 연결을 먼저 설정하세요.", Toast.LENGTH_LONG);
//        }

    }


    private void openBT() {
//        UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); //Standard SerialPortService ID
//        try {
//            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
//            mmSocket.connect();
//            mmOutputStream = mmSocket.getOutputStream();
//            mmInputStream = mmSocket.getInputStream();
//
//            beginListenForData();
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }

    private void beginListenForData() {
        final Handler handler = new Handler();
        final byte delimiter = 10; //This is the ASCII code for a newline character

        stopWorker = false;
        readBufferPosition = 0;
        readBuffer = new byte[1024];
        workerThread = new Thread(new Runnable()
        {
            public void run()
            {
                while(!Thread.currentThread().isInterrupted() && !stopWorker)
                {
                    try
                    {
                        int bytesAvailable = mmInputStream.available();
                        if(bytesAvailable > 0)
                        {
                            byte[] packetBytes = new byte[bytesAvailable];
                            mmInputStream.read(packetBytes);
                            for(int i=0;i<bytesAvailable;i++)
                            {
                                byte b = packetBytes[i];
                                if(b == delimiter)
                                {
                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, "UTF-8");
                                    readBufferPosition = 0;

                                    handler.post(new Runnable()
                                    {
                                        public void run()
                                        {
                                            clearControll();
                                            etSurveyNo.setText(data);
                                            Log.e("data", data);

                                            // searchAssetNo();
                                            readingYn = 1;
                                        }
                                    });
                                }
                                else
                                {
                                    readBuffer[readBufferPosition++] = b;
                                }
                            }
                        }
                    }
                    catch (IOException ex)
                    {
                        stopWorker = true;
                    }
                }
            }
        });

        workerThread.start();

    }

    private void closeBT() {
        try {
            stopWorker = true;
            mmOutputStream.close();
            mmInputStream.close();
            mmSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void clearControll() {
        clearControll(0);
    }

    /**
     * 구분값이
     * 0이면 밑에것만
     * 1이면 자산번호까지
     * 2이면 전부다
     * @param gubn
     */
    private void clearControll(int gubn) {
        // etLabelNo    .setText("");
        // etProdNm     .setText("");
        if (gubn > 1) {
            etSurveyNo   .setText("");
            disableControll(0);
        }
        if (gubn > 0) {
            etBarcodeNo    .setText("");
        }
        etGubunNm    .setText("");
        etModelNm    .setText("");
        etSpecNm     .setText("");
        etSerialNum  .setText("");
        readingYn    = 0;
        // assetInfoCnt = 0;
        // assetPicCnt  = 0;
        // tvAssetInfoCnt.setText(assetInfoCnt + "");
        // tvAssetPicCnt.setText(assetPicCnt + "");

        if (gubn > 2) {
            clearImages(3);
        } else {
            clearImages(0);
        }

    }

    private void clearImages(int gubn) {
        if (gubn > 1) {
            picImage.setImageBitmap(null);
            mFilePic = new ArrayList<File>();
        }
        ((GridLayout)findViewById(R.id.infoGrid)).removeAllViewsInLayout();
        mFileList = new ArrayList<File>();
    }

    private void clearImages() {
        clearImages(0);
    }

    private boolean getNetWorkState() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }

        return false;
    }


    private void insertFileList() {

        if (getNetWorkState() && ( mFileList.size() > 0 || mFilePic.size() > 0 )) {
            try {
                List list = new ArrayList();
                String id = String.valueOf(new Date().getTime());

                String url = OnbridPreference.getString(SurveyNomalActivity.this, "url") + "/insertFileList";

                File file;
                Map fileMap;
                for (int i=0; i<mFileList.size(); i++) {
                    file = mFileList.get(i);
                    fileMap = new HashMap();
                    fileMap.put("UNIVNO"       , OnbridPreference.getString(SurveyNomalActivity.this, "univNo"));
                    fileMap.put("CAMPUSNO"     , OnbridPreference.getString(SurveyNomalActivity.this, "campusNo"));
                    fileMap.put("CATEGORYITEM" , etSurveyNo.getText().toString());
                    fileMap.put("ID"           , id + i);
                    fileMap.put("CATEGORY"     , "SURVEYASSET_ASSET_" + file.getName().split("_")[1]);
                    fileMap.put("NAME"         , etSurveyNo.getText().toString()+"_"+i);
                    fileMap.put("NAMER"        , file.getName());
                    fileMap.put("EXT"          , JPEG_FILE_SUFFIX);
                    fileMap.put("FILESIZE"     , (file.length()/1024));
                    fileMap.put("PATH"         , "");
                    fileMap.put("TYPE"         , "image/jpeg");
                    fileMap.put("BIGO", "");
                    fileMap.put("LOGINEMP", OnbridPreference.getString(SurveyNomalActivity.this, "loginEmp"));

                    list.add(fileMap);
                }
                for (int i=0; i<mFilePic.size(); i++) {
                    file = mFilePic.get(i);
                    fileMap = new HashMap();
                    fileMap.put("UNIVNO"       , OnbridPreference.getString(SurveyNomalActivity.this, "univNo"));
                    fileMap.put("CAMPUSNO"     , OnbridPreference.getString(SurveyNomalActivity.this, "campusNo"));
                    fileMap.put("CATEGORYITEM" , etSurveyNo.getText().toString());
                    fileMap.put("ID"           , id + i + i);
                    fileMap.put("CATEGORY"     , "SURVEYASSET_ASSET_" + file.getName().split("_")[1]);
                    fileMap.put("NAME"         , etSurveyNo.getText().toString()+"_"+i);
                    fileMap.put("NAMER"        , file.getName());
                    fileMap.put("EXT"          , JPEG_FILE_SUFFIX);
                    fileMap.put("FILESIZE"     , (file.length()/1024));
                    fileMap.put("PATH"         , "");
                    fileMap.put("TYPE"         , "image/jpeg");
                    fileMap.put("BIGO", "");
                    fileMap.put("LOGINEMP", OnbridPreference.getString(SurveyNomalActivity.this, "loginEmp"));

                    list.add(fileMap);
                }


                Map param = new HashMap();
                param.put("list", list);
                Log.e("param", new Gson().toJson(param));

                String _message = new Gson().toJson(param);
                ByteArrayEntity entity = new ByteArrayEntity(_message.getBytes("UTF-8"));

                AsyncHttpClient client = new AsyncHttpClient();
                client.post(getParent(), url, entity, "application/json", new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        //super.onSuccess(statusCode, headers, response);
                        Log.e("FileList.onSuccess", response.toString());

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        //super.onFailure(statusCode, headers, responseString, throwable);
                        //Toast.makeText(getApplicationContext(), "정보가 없습니다.", Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                uploadImageFile();
            }
        } else {
            // Toast.makeText(getApplicationContext(), "네트워크 연결상태를 확인해 주세요.", Toast.LENGTH_LONG).show();
        }

    }

    private void uploadImageFile() {

        String url = "/uploadFile";
        File file;

        try {
            RequestParams params = new RequestParams();
            params.put("UNIVNO"    , OnbridPreference.getString(SurveyNomalActivity.this, "univNo"));
            params.put("CAMPUSNO"  , OnbridPreference.getString(SurveyNomalActivity.this, "campusNo"));
            params.put("CAMPUSNO"  , OnbridPreference.getString(SurveyNomalActivity.this, "campusNo"));
            params.put("LOGINEMP"  , OnbridPreference.getString(SurveyNomalActivity.this, "loginEmp"));

            for (int i=0; i<mFileList.size(); i++) {

                file = mFileList.get(i);
                params.put("FILE"      , file);
                params.put("CATEGORY"  , "SURVEYASSET_ASSET_" + file.getName().split("_")[1]);

                OnbridRestClient.put(this, url, params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        Log.e("onSuccess", response.toString());
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        Log.e("onFailure", errorResponse.toString());
                    }
                });

            }

            for (int i=0; i<mFilePic.size(); i++) {
                file = mFilePic.get(i);

                if ( etSurveyNo.getText().toString().equalsIgnoreCase( file.getName().split("_")[0] )) {
                    params.put("FILE"      , file);
                    params.put("CATEGORY"  , "SURVEYASSET_ASSET_" + file.getName().split("_")[1]);

                    OnbridRestClient.put(this, url, params, new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            super.onSuccess(statusCode, headers, response);
                            Log.e("onSuccess", response.toString());
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            super.onFailure(statusCode, headers, throwable, errorResponse);
                            Log.e("onFailure", errorResponse.toString());
                        }
                    });
                }
            }



        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            clearImages();
        }

    }


    /**
     * 구분 0: 조사번호
     * 구분 1: 재물번호
     * @param gubn
     */
    private void searchAssetNo(final int gubn) {

        if (gubn < 1) {
            clearControll(1);
            if (etSurveyNo.getText().toString().length() < 1) {
                Toast.makeText(getApplicationContext(), "조사번호를 입력해주세요.", Toast.LENGTH_LONG).show();
                return;
            }
            etSurveyNo.setText(etSurveyNo.getText().toString().toUpperCase());

            disableControll(1);

        } else {
            clearControll(0);
            if (etBarcodeNo.getText().toString().length() < 1) {
                Toast.makeText(getApplicationContext(), "재물번호를 입력해주세요.", Toast.LENGTH_LONG).show();
                return;
            }
            etBarcodeNo.setText(etBarcodeNo.getText().toString().toUpperCase());
        }

        /* 2015-12-07 강명원  기존 기초 조사한것도 조회해야 하기 때문에 임시라벨번호도 조회한다
        if ( etSurveyNo.getText().toString().toUpperCase().contains(OnbridPreference.getString(this, "surveyPrefix").toUpperCase() ) ) {
            return;
        }*/



        if (getNetWorkState()) {

            try {

                // stopWorker = true;

                String url = OnbridPreference.getString(SurveyNomalActivity.this, "url") + "/ws/sa/sabase/selectSurveyNoInfo";
                Map param = new HashMap();
                param.put("UNIVNO", OnbridPreference.getString(SurveyNomalActivity.this, "univNo"));
                param.put("CAMPUSNO", OnbridPreference.getString(SurveyNomalActivity.this, "campusNo"));
                if (gubn > 0) {
                    param.put("SURVEYNO", etBarcodeNo.getText().toString().trim());
                } else {
                    param.put("SURVEYNO", etSurveyNo.getText().toString().trim());
                }
                param.put("LOGINEMP", OnbridPreference.getString(SurveyNomalActivity.this, "loginEmp"));

                String _message = new Gson().toJson(param);
                Log.e("_message", _message);
                ByteArrayEntity entity = new ByteArrayEntity(_message.getBytes("UTF-8"));

                AsyncHttpClient client = new AsyncHttpClient();
                client.post(getParent(), url, entity, "application/json", new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        // super.onSuccess(statusCode, headers, response);
                        try {
                            Log.e("searchAssetNo", response.toString());
                            if (response.length() < 1) {
                                Toast.makeText(getApplicationContext(), "재물정보가 없습니다.", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            if (response.get("BARCODE").toString()   != "null") etBarcodeNo.setText(response.get("BARCODE").toString());
                            if (response.get("GUBUNNAME").toString() != "null") etGubunNm.setText(response.get("GUBUNNAME").toString());
                            if (response.get("MODELNAME").toString() != "null") etModelNm.setText(response.get("MODELNAME").toString());
                            if (response.get("SPEC").toString()      != "null") etSpecNm.setText(response.get("SPEC").toString());
                            if (response.get("SERIALNUM").toString() != "null") etSerialNum.setText(response.get("SERIALNUM").toString());

                            if (gubn < 1) {
                                //disableControll(0);
                                readingYn = (int) response.get("READINGYN");
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            etGubunNm.requestFocus();
                        }

                    }


                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        //super.onFailure(statusCode, headers, responseString, throwable);
                        Toast.makeText(getApplicationContext(), "재물정보가 없습니다.", Toast.LENGTH_SHORT).show();
                    }

                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Toast.makeText(getApplicationContext(), "네트워크 연결상태를 확인해 주세요.", Toast.LENGTH_LONG).show();
        }
    }



    ProgressDialog progressDialog;
    public void showProgress(boolean show ){

        if( show ){

            try{
                progressDialog = new ProgressDialog( this );
                progressDialog.setCancelable(false);
                progressDialog.setMessage("잠시만 기다려주세요.");
                progressDialog.show();
            }catch(Exception e){

            }


        }else{

            if( progressDialog != null ){
                progressDialog.dismiss();
            }

        }

    }


    private static class UsbSerialHandler extends Handler {
        private final WeakReference<SurveyNomalActivity> mActivity;

        public UsbSerialHandler(SurveyNomalActivity activity) { mActivity = new WeakReference<SurveyNomalActivity>(activity); }

        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case UsbService.MESSAGE_FROM_SERIAL_PORT:
                    String data = ((String) msg.obj).trim();
                    if (data.length() > 0) {
                        // Log.e("USB_DATA", data + "/" + data.length());
                        mActivity.get().getHandleMessage(data);
                    }
                    break;
            }
        }
    }

    private void getHandleMessage( String data ) {


        if (data.toUpperCase().indexOf(OnbridPreference.getString(this, "surveyPrefix").toUpperCase()) > -1) {
            clearControll(1);
            etSurveyNo.setText(data);
            searchAssetNo(0);
        } else {
            if (!etBarcodeNo.isEnabled()) {
                Toast.makeText(this, "조사번호를 먼저 스캔하세요.", Toast.LENGTH_LONG).show();
                return;
            }
            // Log.e("getHandleMessage", data);
            clearControll();
            etBarcodeNo.setText(data);
            searchAssetNo(1);
            readingYn = 1;
        }


    }

    private void startService(Class<?> service, ServiceConnection serviceConnection, Bundle extras)
    {
        if(UsbService.SERVICE_CONNECTED == false)
        {
            Intent startService = new Intent(this, service);
            if(extras != null && !extras.isEmpty())
            {
                Set<String> keys = extras.keySet();
                for(String key: keys)
                {
                    String extra = extras.getString(key);
                    startService.putExtra(key, extra);
                }
            }
            startService(startService);
        }
        Intent bindingIntent = new Intent(this, service);
        bindService(bindingIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    private void setFilters()
    {
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbService.ACTION_USB_PERMISSION_GRANTED);
        filter.addAction(UsbService.ACTION_NO_USB);
        filter.addAction(UsbService.ACTION_USB_DISCONNECTED);
        filter.addAction(UsbService.ACTION_USB_NOT_SUPPORTED);
        filter.addAction(UsbService.ACTION_USB_PERMISSION_NOT_GRANTED);
        registerReceiver(mUsbReceiver, filter);
    }

    /*
	 * Notifications from UsbService will be received here.
	 */
    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context arg0, Intent arg1)
        {
            if(arg1.getAction().equals(UsbService.ACTION_USB_PERMISSION_GRANTED)) // USB PERMISSION GRANTED
            {
                Toast.makeText(arg0, "USB 시리얼 스캐너 준비됨", Toast.LENGTH_SHORT).show();
            }else if(arg1.getAction().equals(UsbService.ACTION_USB_PERMISSION_NOT_GRANTED)) // USB PERMISSION NOT GRANTED
            {
                Toast.makeText(arg0, "USB 통신 권한없음", Toast.LENGTH_SHORT).show();
            }else if(arg1.getAction().equals(UsbService.ACTION_NO_USB)) // NO USB CONNECTED
            {
                Toast.makeText(arg0, "USB 연결안됨", Toast.LENGTH_SHORT).show();
            }else if(arg1.getAction().equals(UsbService.ACTION_USB_DISCONNECTED)) // USB DISCONNECTED
            {
                Toast.makeText(arg0, "USB 분리됨", Toast.LENGTH_SHORT).show();
            }else if(arg1.getAction().equals(UsbService.ACTION_USB_NOT_SUPPORTED)) // USB NOT SUPPORTED
            {
                Toast.makeText(arg0, "USB 장치 지원안함", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private final ServiceConnection usbConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName arg0, IBinder arg1)
        {
            usbService = ((UsbService.UsbBinder) arg1).getService();
            usbService.setHandler(usbSerialHandler);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0)
        {
            usbService = null;
        }
    };

}




