package com.onbrid.onams.cau.util;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.entity.ByteArrayEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;

/**
 * Created by Enable on 2015-12-18.
 */
public class OnbridRestClient {

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void post(Context context, String url, JSONObject params, AsyncHttpResponseHandler responseHandler) throws UnsupportedEncodingException {
        ByteArrayEntity entity = new ByteArrayEntity(params.toString().getBytes("UTF-8"));
        entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

        client.post(context, getAbsoluteUrl(context, url), entity, "application/json", responseHandler);

    }


    public static void put(Context context, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl(context, url), params, responseHandler);
    }

    // public static void puts

    private static String getAbsoluteUrl(Context context, String relativeUrl) {
        return OnbridPreference.getString(context, "url") + relativeUrl;
    }



}
