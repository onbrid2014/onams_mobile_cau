package com.onbrid.onams.cau.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.onbrid.onams.cau.SearchCode;
import com.onbrid.onams.cau.dialog.OnbridProgressDialog;
import com.onbrid.onams.cau.model.Code;
import com.onbrid.onams.cau.model.Option;
import com.onbrid.onams.cau.util.OnbridPreference;
import com.onbrid.onams.cau.R;
import com.onbrid.onams.cau.SurveyDeptScan;
import com.onbrid.onams.cau.adaptor.AssetSearchOptionAdaptor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 부서재물조사 메인 화면
 */
public class SurveyAssetScan extends Fragment {

    ListView listviewOption;
    AssetSearchOptionAdaptor assetSearchOptionAdaptor;



    Map searchOption;

    View rootView;

    Menu menu;

    Toast toast;

    // 리스트뷰 롱클릭 이벤트 포지션
    private int selectedPos = -1;


    private final static String OPTION_PLAN     =   "PLAN";
    private final static String OPTION_PLACE    =   "PLACE";
    private final static String OPTION_DEPT     =   "DEPT";
//    private final static String OPTION_SCAN_YN  =   "SCAN_YN";



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_survey_asset, container, false);

        /* enable onCreateOptionsMenu */
        setHasOptionsMenu(true);

        this.initListviewOption();

        // System.out.println("Flagment2 onCreateView");

        return rootView;
    }

//    @Override
//    public boolean onMenuItemClick(MenuItem item) {
//
//
//        if( item.getItemId() == R.id.action_search ){
//
//            System.out.println( item.getItemId() +" > 조회!");
//            new DataManager().execute();
//
//        }else if( item.getItemId() == R.id.action_clear ){
//
//            System.out.println( item.getItemId() +" > 초기화!");
//
//            int size = assetSearchOptionAdaptor.getCount();
//            for( int i=0; i<size ; i++ ){
//                ((Option) assetSearchOptionAdaptor.getItem(i)).clear();
//            }
//
//            this.initParam();
//            assetSearchOptionAdaptor.notifyDataSetChanged();
//
//        }
//
//        return false;
//    }

    @Override
    public void onStart() {
        super.onStart();

        System.out.println("Flagment onStart");
    }


    @Override
    public void onPause() {
        super.onPause();
        System.out.println("Flagment onPause");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        System.out.println("Flagment onDestroy");
    }


    private void initParam(){
        /* 재물조회 파라미터 초기화 */
        if(searchOption==null){
            searchOption = new HashMap();
        }
        searchOption.put("UNIVNO"    , OnbridPreference.getString(getActivity(), "univNo"));
        searchOption.put("CAMPUSNO"  , OnbridPreference.getString(getActivity(), "campusNo"));
        searchOption.put("PAGENUM", 1);
        searchOption.put("PAGEROW", 20);
        searchOption.put("LOGINEMP", "1001");

        searchOption.put("PLACENO", "");
        searchOption.put("PLACENAME", "");
        searchOption.put("DEPTNO", "");
        searchOption.put("DEPTNAME", "");
        searchOption.put("PLANNO", OnbridPreference.getString(getActivity(), "planNo"));
    }

    private void initListviewOption(){


        this.initParam();

        /* 조회조건 추가 */
        ArrayList<Option> options = new ArrayList<>();
//        options.add(new Option("0000", "조사명", OPTION_PLAN, R.drawable.ic_action_labels));
        options.add(new Option("0001", "부서정보", OPTION_DEPT, R.drawable.ic_action_group));
        options.add(new Option("0002", "위치정보", OPTION_PLACE, R.drawable.ic_action_place));

        options.get(0).setRequired(true);
//        options.get(1).setRequired(true);

        /* 검색조건 리스트뷰/아탑터 초기화 */
        listviewOption              =   (ListView) rootView.findViewById(R.id.listviewOption);
        assetSearchOptionAdaptor    =   new AssetSearchOptionAdaptor( getActivity(), options);
        listviewOption.setAdapter(assetSearchOptionAdaptor);


        /* 검색조건 리스트 클릭 이벤트 */
        listviewOption.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Option option = (Option) listviewOption.getItemAtPosition(position);

                if (option.getType() == OPTION_PLACE) {
                    if (searchOption.get("DEPTNO").toString().equalsIgnoreCase("")) {
                        Toast.makeText(getContext(), "조사부서를 먼저 선택하세요.", Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                //  조사상태 콜백
                Intent intent = new Intent(getActivity(), SearchCode.class);
                intent.putExtra("position", position);
                intent.putExtra("type", option.getType());
                if (option.getType() == OPTION_PLACE) {
                    intent.putExtra("DEPTNO", searchOption.get("DEPTNO").toString());
                }

                startActivityForResult(intent, 1);

            }


        });

        /* 검색조건 리스트 롱!! 클릭 이벤트 ## 검색 조건을 초기화한다. */
        listviewOption.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                String title = "";

                selectedPos = position;

                switch (selectedPos) {
//                    case 0 : title = "조사계획"; break;
                    case 0 : title = "부서"; break;
                    case 1 : title = "장소"; break;
                }

                AlertDialog.Builder alertDlg = new AlertDialog.Builder(view.getContext());
                alertDlg.setTitle(title + "조건 초기화");

                alertDlg.setPositiveButton("예", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        ((Option) assetSearchOptionAdaptor.getItem(selectedPos)).clear();
                        assetSearchOptionAdaptor.notifyDataSetChanged();

                        switch (selectedPos) {
//                            case 0 : searchOption.put("PLANNO", ""); break;
                            case 0 :
                                searchOption.put("DEPTNO", "");
                                searchOption.put("DEPTNAME", "");
                                break;
                            case 1 :
                                searchOption.put("PLACENO", "");
                                searchOption.put("PLACENAME", "");
                                break;
                        }

                        dialog.dismiss();
                    }
                });

                alertDlg.setNegativeButton("아니오", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alertDlg.show();

                return false;
            }
        });

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_fragment_main3, menu);

//        ((MenuItem) menu.getItem(0)).setOnMenuItemClickListener(this);
//        ((MenuItem) menu.getItem(1)).setOnMenuItemClickListener(this);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        System.out.println("onOptionsItemSelected : " + item.getItemId());

        switch ( item.getItemId() ){

            //  검색버 튼
            case    R.id.action_start :

                /*
                    TODO : Validate 처리하기...
                    1. 조사명, 부서 필수 조건으로..
                 */
                if ( searchOption.get("PLANNO").toString().equalsIgnoreCase("") ||
                        searchOption.get("DEPTNO").toString().equalsIgnoreCase("") ) {
                    Toast.makeText(getActivity(), "필수항목을 입력하세요.", Toast.LENGTH_SHORT).show();
                    break;
                }

                Intent intent = new Intent(getActivity(), SurveyDeptScan.class);
                intent.putExtra("PLANNO",       OnbridPreference.getString(getActivity(), "planNo")   );
                intent.putExtra("PLACENO",      searchOption.get("PLACENO").toString()  );
                intent.putExtra("PLACENAME",    searchOption.get("PLACENAME").toString());
                intent.putExtra("DEPTNO",       searchOption.get("DEPTNO").toString()   );
                intent.putExtra("DEPTNAME",     searchOption.get("DEPTNAME").toString() );

                startActivity(intent);

                break;


            //  초기화 버튼
            case    R.id.action_clear :

                int size = assetSearchOptionAdaptor.getCount();
                for( int i=0; i<size ; i++ ){
                    ((Option) assetSearchOptionAdaptor.getItem(i)).clear();
                }

                this.initParam();
                assetSearchOptionAdaptor.notifyDataSetChanged();

                toast   =   Toast.makeText(getActivity(), "검색조건이 초기화 되었습니다.", Toast.LENGTH_SHORT);
                toast.show();

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Toast.makeText(getActivity(), "onActivityResult", Toast.LENGTH_SHORT).show();
        Code returnCode = data.hasExtra("code")? (Code) data.getSerializableExtra("code"):null;
        String type     = data.getStringExtra("type");
        int position    = data.getIntExtra("position", -1);

        if( returnCode == null || position == -1 ){
            return;
        }

        ((Option) assetSearchOptionAdaptor.getItem( position)).set(returnCode);

        switch( type ){
            case OPTION_PLAN  :
                searchOption.put("PLANNO",      returnCode.getName() );
                break;
            case OPTION_PLACE :
                searchOption.put("PLACENO",     returnCode.getNo() );
                searchOption.put("PLACENAME",   returnCode.getName() );
                break;
            case OPTION_DEPT  :
                searchOption.put("DEPTNO",      returnCode.getNo() );
                searchOption.put("DEPTNAME",    returnCode.getName() );
                break;
        }

        assetSearchOptionAdaptor.notifyDataSetChanged();

    }




    OnbridProgressDialog progressDialog;
    public void showProgress(boolean show ){

        if( show ){

            try{
                progressDialog = new OnbridProgressDialog( getActivity() );
                progressDialog.show();
            }catch(Exception e){}

        }else{

            if( progressDialog != null ){
                progressDialog.dismiss();
            }

        }

    }




}
