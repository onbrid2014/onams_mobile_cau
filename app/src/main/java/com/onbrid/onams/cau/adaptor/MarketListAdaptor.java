package com.onbrid.onams.cau.adaptor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.onbrid.onams.cau.model.MarketItem;
import com.onbrid.onams.cau.R;

import java.util.ArrayList;

/**
 * Created by Enable on 2016-03-10.
 */
public class MarketListAdaptor extends ArrayAdapter {

    private Context context;

    public MarketListAdaptor(Context context, ArrayList<MarketItem> assetList) {
        super(context, 0, assetList);
        this.context = context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final MarketItem item = (MarketItem) getItem(position);
        ViewHolder viewHolder;

        if ( convertView == null ) {

            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_layout_market, parent, false);

            /* UI 엘리먼트를 매핑 */
            viewHolder.assetNo      = (TextView) convertView.findViewById(R.id.tvAssetNo);
            viewHolder.assetName    = (TextView) convertView.findViewById(R.id.tvAssetName);
            viewHolder.writeDate    = (TextView) convertView.findViewById(R.id.tvWriteDate);
            viewHolder.deptName     = (TextView) convertView.findViewById(R.id.tvDeptName);
            viewHolder.placeName    = (TextView) convertView.findViewById(R.id.tvPlaceName);

            viewHolder.ivDelete     = (ImageView) convertView.findViewById(R.id.ivDelete);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        /* UI 엘리먼트에 값을 바인딩 */
        viewHolder.assetNo.setText(item.getAssetNo());
        viewHolder.assetName.setText(item.getAssetName());
        viewHolder.writeDate.setText(item.getWriteDate());
        viewHolder.deptName.setText(item.getFromDeptName());
        viewHolder.placeName.setText(item.getFromPlaceName());

        viewHolder.setStatusDraw(item.getConfState());


        return convertView;
    }

    private static class ViewHolder {
        TextView assetNo;
        TextView assetName;
        TextView writeDate;
        TextView deptName;
        TextView placeName;

        ImageView ivDelete;

        private void setStatusDraw( int status ){
            if( status > -1 ){
                this.ivDelete.setImageBitmap(null);
            } else {
                this.ivDelete.setImageResource( R.drawable.ic_action_cancel_light );
            }
        }

    }
} // class
