package com.onbrid.onams.cau.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.onbrid.onams.cau.usb.UsbService;
import com.onbrid.onams.cau.util.OnbridPreference;
import com.onbrid.onams.cau.util.OnbridResponseHandler;
import com.onbrid.onams.cau.util.OnbridRestClient;
import com.onbrid.onams.cau.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;

import cz.msebera.android.httpclient.Header;


public class SearchAsset extends Fragment {

    private View rootView;


    //private UsbSerialHandler usbSerialHandler;
    private UsbService usbService;


    private Button btnSearch;

    private EditText  etAssetNo;
    private ImageView assetImage;
    private TextView  assetNo;
    private TextView  assetName;
    private TextView  assetPlace;
    private TextView  assetDept;
    private TextView  assetModel;
    private TextView  assetSpec;
    private TextView  assetDate;
    private TextView  assetAmount;
    private TextView  assetState;
    private TextView  assetGubn;
    //private TextView  assetBigo;

    private View.OnClickListener btnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnSearch :
                    getAssetDetail();
                    break;
            }
        }
    };

    private TextView.OnEditorActionListener etListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

            getAssetDetail();
            return true;
        }
    };


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // return super.onCreateView(inflater, container, savedInstanceState);

        rootView = inflater.inflate(R.layout.fragment_search_asset, container, false);

        /* enable onCreateOptionsMenu */
        setHasOptionsMenu(true);

        initControll();

        return rootView;
    }

    private void initControll() {
        btnSearch = (Button)  rootView.findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(btnListener);

        etAssetNo    = (EditText)  rootView.findViewById(R.id.etAssetNo);
        etAssetNo.setOnEditorActionListener(etListener);

        assetImage   = (ImageView) rootView.findViewById(R.id.assetImage);
        assetNo      = (TextView)  rootView.findViewById(R.id.assetNo    );
        assetName    = (TextView)  rootView.findViewById(R.id.assetName  );
        assetPlace   = (TextView)  rootView.findViewById(R.id.assetPlace );
        assetDept    = (TextView)  rootView.findViewById(R.id.assetDept  );
        assetModel   = (TextView)  rootView.findViewById(R.id.assetModel );
        assetSpec    = (TextView)  rootView.findViewById(R.id.assetSpec  );
        assetDate    = (TextView)  rootView.findViewById(R.id.assetDate  );
        assetAmount  = (TextView)  rootView.findViewById(R.id.assetAmount);
        assetState   = (TextView)  rootView.findViewById(R.id.assetState );
        assetGubn    = (TextView)  rootView.findViewById(R.id.assetGubn  );
        //assetBigo    = (TextView)  rootView.findViewById(R.id.assetBigo  );
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_fragment_main1, menu);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch ( item.getItemId() ){
            case R.id.action_search :
                getAssetDetail();
                break;
            case R.id.action_clear :
                setText(assetNo, "" );
                setText(assetName, "" );
                setText(assetPlace, "" );
                setText(assetDept, "" );
                setText(assetModel, "" );
                setText(assetSpec, "" );
                setText(assetDate, "" );
                setText(assetAmount, "" );
                setText(assetState, "" );
                setText(assetGubn, "" );
                //setText(assetBigo, "" );
                break;
        }


        return super.onOptionsItemSelected(item);
    }

    private void getAssetDetail() {

        try {
            String url = "/ws/pda/asset/select";

            // {DATABLOCK=[{UNIVNO=0001, LOGINEMP=1001, PLANNO=20150025, PDANO=101, CAMPUSNO=01, ASSETNO=201500000019}]}
            JSONObject param = new JSONObject();
            JSONArray dataBlock = new JSONArray();
            JSONObject paramInner = new JSONObject();

            paramInner.put("UNIVNO"  , OnbridPreference.getString(getContext(), "univNo"));
            paramInner.put("CAMPUSNO", OnbridPreference.getString(getContext(), "campusNo"));
            paramInner.put("ASSETNO" , etAssetNo.getText().toString());
            dataBlock.put(paramInner);
            param.put("DATABLOCK", dataBlock);
            // Log.d("param", param.toString());

            OnbridRestClient.post(getContext(), url, param, new OnbridResponseHandler(getContext()) {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Log.d("onSuccess", response.toString());
                    // {"USESTATE":0,"PRICE":42000,"USEKINDNAME":"공기구비품","ASSETSTATENAME2":null,"BIGO":null,"ASSETTYPENAME":"공기구비품","CAMPUSNO":"01","GUBUNNAME":"책상",
                    // "DEPTNAME":"기획 관리부","ASSETNO":"201500000003","SPEC":"1200*700*720","DEPTNO":"0001","ASSETSTATE":255,"BARCODE":"2015000003","QTY":1,"ASSETNAME":"책상",
                    // "NOMALGUBUN":0,"PLACENAME":"기획 관리부 사무실","NOMALGUBUNNAME":null,"GUBUNNO":"0001","MODELNAME":"703 탑책상","UNIVNO":"0001","CAMPUSNAME":"OnBrid-대학",
                    // "AMOUNT":42000,"ASSETSTATENAME":"정상","WRITEDATE":"20150907","ACCGUBUNNAME":null,"ACCGUBUN":null,"ROWNUM":1,"ASSETTYPE":"0008","OLDASSETNO":null,"USEKIND":"0016","PLACENO":"0003","DUDATE":null}
                    try {
                        setText(assetNo, response.getString("ASSETNO"));
                        setText(assetName, response.getString("ASSETNAME"));
                        setText(assetPlace, response.getString("PLACENAME"));
                        setText(assetDept, response.getString("DEPTNAME"));
                        setText(assetModel, response.getString("MODELNAME"));
                        setText(assetSpec, response.getString("SPEC"));
                        setText(assetDate, response.getString("WRITEDATE"), "date");
                        setText(assetAmount, response.getString("AMOUNT"), "decimal");
                        setText(assetState, response.getString("ASSETSTATENAME"));
                        setText(assetGubn, response.getString("ASSETTYPENAME"));
                        //setText(assetBigo, response.getString("BIGO"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * EditText, TextView 에 text(내용)을 셋한다.
     * @param item
     * @param val
     */
    private void setText(TextView item, Object val) {
        item.setText(val.toString().equalsIgnoreCase("null") ? "-" : val.toString());
    }
    private void setText(TextView item, Object val, String type) {
        String value = "";

        if (type.equalsIgnoreCase("date")) {
            value = val.toString().substring(0,4) + "-" + val.toString().substring(4,6) + "-" + val.toString().substring(6,8);
        } else if (type.equalsIgnoreCase("decimal")) {
            DecimalFormat format = new DecimalFormat("#,###");
            value = (String) format.format(Integer.parseInt(val.toString()));
        } else {
            value = (String) val;
        }

        setText(item, value);
    }





    /**
     * UsbSerial 스캔 이벤트 결과 처리
     * @param data
     */
    private void getHandleMessage( String data ) {
        setText(etAssetNo, data.trim());
        getAssetDetail();
    }

    /**
     * Inner Class UsbSerialHandler
     */
//    private static class UsbSerialHandler extends Handler {
//        private final WeakReference<SurveyDeptScan> mActivity;
//
//        public UsbSerialHandler(SurveyDeptScan activity) { mActivity = new WeakReference<SurveyDeptScan>(activity); }
//
//        @Override
//        public void handleMessage(Message msg) {
//
//            switch (msg.what) {
//                case UsbService.MESSAGE_FROM_SERIAL_PORT:
//                    String data = ((String) msg.obj).trim();
//                    if (data.length() > 0) {
//                        // Log.e("USB_DATA", data + "/" + data.length());
//                        mActivity.get().getHandleMessage(data);
//                    }
//                    break;
//            }
//        }
//    }
//
//    private void startService(Class<?> service, ServiceConnection serviceConnection, Bundle extras)
//    {
//        if(UsbService.SERVICE_CONNECTED == false)
//        {
//            Intent startService = new Intent(this, service);
//            if(extras != null && !extras.isEmpty())
//            {
//                Set<String> keys = extras.keySet();
//                for(String key: keys)
//                {
//                    String extra = extras.getString(key);
//                    startService.putExtra(key, extra);
//                }
//            }
//            startService(startService);
//        }
//        Intent bindingIntent = new Intent(this, service);
//        bindService(bindingIntent, serviceConnection, Context.BIND_AUTO_CREATE);
//    }
//
//    private void setFilters()
//    {
//        IntentFilter filter = new IntentFilter();
//        filter.addAction(UsbService.ACTION_USB_PERMISSION_GRANTED);
//        filter.addAction(UsbService.ACTION_NO_USB);
//        filter.addAction(UsbService.ACTION_USB_DISCONNECTED);
//        filter.addAction(UsbService.ACTION_USB_NOT_SUPPORTED);
//        filter.addAction(UsbService.ACTION_USB_PERMISSION_NOT_GRANTED);
//        registerReceiver(mUsbReceiver, filter);
//    }
//
//    /*
//	 * Notifications from UsbService will be received here.
//	 */
//    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver()
//    {
//        @Override
//        public void onReceive(Context arg0, Intent arg1)
//        {
//            if(arg1.getAction().equals(UsbService.ACTION_USB_PERMISSION_GRANTED)) // USB PERMISSION GRANTED
//            {
//                Toast.makeText(arg0, "USB 시리얼 스캐너 준비됨", Toast.LENGTH_SHORT).show();
//            }else if(arg1.getAction().equals(UsbService.ACTION_USB_PERMISSION_NOT_GRANTED)) // USB PERMISSION NOT GRANTED
//            {
//                Toast.makeText(arg0, "USB 통신 권한없음", Toast.LENGTH_SHORT).show();
//            }else if(arg1.getAction().equals(UsbService.ACTION_NO_USB)) // NO USB CONNECTED
//            {
//                Toast.makeText(arg0, "USB 연결안됨", Toast.LENGTH_SHORT).show();
//            }else if(arg1.getAction().equals(UsbService.ACTION_USB_DISCONNECTED)) // USB DISCONNECTED
//            {
//                Toast.makeText(arg0, "USB 분리됨", Toast.LENGTH_SHORT).show();
//            }else if(arg1.getAction().equals(UsbService.ACTION_USB_NOT_SUPPORTED)) // USB NOT SUPPORTED
//            {
//                Toast.makeText(arg0, "USB 장치 지원안함", Toast.LENGTH_SHORT).show();
//            }
//        }
//    };
//
//    private final ServiceConnection usbConnection = new ServiceConnection()
//    {
//        @Override
//        public void onServiceConnected(ComponentName arg0, IBinder arg1)
//        {
//            usbService = ((UsbService.UsbBinder) arg1).getService();
//            usbService.setHandler(usbSerialHandler);
//        }
//
//        @Override
//        public void onServiceDisconnected(ComponentName arg0)
//        {
//            usbService = null;
//        }
//    };

}
