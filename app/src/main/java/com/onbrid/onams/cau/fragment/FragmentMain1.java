package com.onbrid.onams.cau.fragment;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.onbrid.onams.cau.DrawerActivity;
import com.onbrid.onams.cau.R;
import com.onbrid.onams.cau.SettingActivity;

public class FragmentMain1 extends Fragment implements MenuItem.OnMenuItemClickListener {
    LinearLayout scanDept;
    LinearLayout scanAsset;

    Toast toast;

    Menu menu;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);



        View rootView = inflater.inflate(R.layout.fragment_main1, container, false);

        scanDept    =   (LinearLayout) rootView.findViewById(R.id.btnScanDept);
        scanDept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DrawerActivity) getActivity()).setFragments(2);
            }
        });

        scanAsset    =   (LinearLayout) rootView.findViewById(R.id.btnSearchAsset);
        scanAsset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((DrawerActivity) getActivity()).setFragments(1);

            }
        });


        setHasOptionsMenu(true);
        System.out.println("Flagment1 onCreateView");


        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        System.out.println("Flagment1 onCreateOptionsMenu");
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        System.out.println("Flagment1 onOptionsItemSelected");

        if( item.getItemId() == R.id.action_settings ){
            Intent intent = new Intent(getActivity(), SettingActivity.class);
            startActivity(intent);
        }else{

            //OnbridPreference.setBoolean(getActivity(), "isAutoLogin", false);
            getActivity().finish();
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if( resultCode == 1 ){
            System.out.println("Fragment1 onActivityResult : " + data.getStringExtra("a"));
        }

    }


    public View.OnClickListener onBtnTest(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent( getActivity(), AssetListActivity.class);
                //startActivityForResult(intent, 1);
            }
        };
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }
}
