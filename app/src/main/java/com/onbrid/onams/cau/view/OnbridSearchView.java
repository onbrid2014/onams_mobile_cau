package com.onbrid.onams.cau.view;

import android.content.Context;
import android.support.v7.widget.SearchView;

/**
 * Created by Enable on 2016. 6. 10..
 */
public class OnbridSearchView extends SearchView {

    private boolean expanded;

    public OnbridSearchView(Context context) {
        super(context);
    }

    @Override
    public void onActionViewExpanded() {
        super.onActionViewExpanded();
        expanded = true;
    }

    @Override
    public void onActionViewCollapsed() {
        super.onActionViewCollapsed();
        expanded = false;
    }

    public boolean isExpanded() {
        return expanded;
    }
}
