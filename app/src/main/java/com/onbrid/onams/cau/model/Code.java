package com.onbrid.onams.cau.model;

import java.io.Serializable;

/**
 * Created by Enable on 2015-10-13.
 */
public class Code implements Serializable {

    private String no;
    private String name;

    private boolean visibleNo   =   true;

    public Code(String no, String name) {
        this.no = no;
        this.name = name;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isVisibleNo() {
        return visibleNo;
    }

    public void setVisibleNo(boolean visibleNo) {
        this.visibleNo = visibleNo;
    }
}
