package com.onbrid.onams.cau.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Enable on 2015-10-30.
 */
public class AssetScanItem extends AssetItem implements Serializable {
    private int status  =   -1;

    public AssetScanItem(String id, String name) {
        super(id, name);
    }

    public AssetScanItem(JSONObject object){
        super(object);
        try {
            int tempStatus = -1;
            if ( object.get("DEPTSURVEYGUBN").toString().equalsIgnoreCase("null") ) {
                setStatus(-1);
            } else {
                setStatus(Integer.parseInt(object.get("DEPTSURVEYGUBN").toString()));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
