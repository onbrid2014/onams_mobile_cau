package com.onbrid.onams.cau.adaptor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.onbrid.onams.cau.R;
import com.onbrid.onams.cau.model.AssetPlusItem;

import java.util.ArrayList;

/**
 * Created by Enable on 2015-11-27.
 * 부서재물조사의 추가조사된 재물 리스트 어답터
 */
public class SurveyPlusListAdaptor extends ArrayAdapter {

    private Context context;

    public SurveyPlusListAdaptor(Context context, ArrayList<AssetPlusItem> assetList) {
        super(context, 0, assetList);
        this.context = context;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final AssetPlusItem item = (AssetPlusItem) getItem(position);
        ViewHolder viewHolder;

        if ( convertView == null ) {

            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_layout_plus, parent, false);

            /* UI 엘리먼트를 매핑 */
            viewHolder.assetNo          = (TextView) convertView.findViewById(R.id.assetNo);
            viewHolder.assetName        = (TextView) convertView.findViewById(R.id.assetName);
            viewHolder.writeTime        = (TextView) convertView.findViewById(R.id.writeTime);
            viewHolder.scanDeptName     = (TextView) convertView.findViewById(R.id.scanDept);
            viewHolder.scanPlaceName    = (TextView) convertView.findViewById(R.id.scanPlace);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        /* UI 엘리먼트에 값을 바인딩 */
        viewHolder.assetNo.setText(item.getId());
        viewHolder.assetName.setText(item.getName());
        viewHolder.writeTime.setText(item.getWriteTime());
        viewHolder.scanPlaceName.setText(item.getScanPlaceName());
        viewHolder.scanDeptName.setText(item.getScanDeptName());



        return convertView;
    }

    public boolean deleteItem(int position) {



        return false;
    }



    private static class ViewHolder {
        TextView assetNo;
        TextView assetName;
        TextView writeTime;
        TextView scanDeptName;
        TextView scanPlaceName;

    }

}
