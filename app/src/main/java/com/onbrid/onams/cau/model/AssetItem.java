package com.onbrid.onams.cau.model;

import org.json.JSONObject;

/**
 * Created by Enable on 2015-10-30.
 */
public class AssetItem extends Asset {

    private int currentPage =   0;
    private int pageRow     =   10;
    private int totalCount  =   0;
    private int totalPage   =   0;


    public AssetItem(String id, String name) {
        super(id, name);
    }

    public AssetItem(JSONObject object) {
        super(object);
        try{
            setCurrentPage((int) object.get("PAGENUM"));
            setTotalPage( (int) object.get("TOTALPAGE") );
            setTotalCount((int) object.get("TOTALROW"));
        }catch(Exception e){

        }
    }


    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPageRow() {
        return pageRow;
    }

    public void setPageRow(int pageRow) {
        this.pageRow = pageRow;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

}
