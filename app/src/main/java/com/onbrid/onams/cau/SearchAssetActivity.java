package com.onbrid.onams.cau;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.onbrid.onams.cau.barcodescan.IntentIntegrator;
import com.onbrid.onams.cau.barcodescan.IntentResult;
import com.onbrid.onams.cau.usb.UsbService;
import com.onbrid.onams.cau.util.OnbridPreference;
import com.onbrid.onams.cau.util.OnbridResponseHandler;
import com.onbrid.onams.cau.util.OnbridRestClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.Set;

import cz.msebera.android.httpclient.Header;

public class SearchAssetActivity extends AppCompatActivity {

    private Toolbar toolbar;

    private UsbSerialHandler usbSerialHandler;
    private UsbService usbService;


    private Button btnScan;
    private Button btnSearch;

    private EditText etAssetNo;
    private ImageView assetImage;
    private TextView assetNo;
    private TextView  assetName;
    private TextView  assetPlace;
    private TextView  assetDept;
    private TextView  assetModel;
    private TextView  assetSpec;
    private TextView  assetDate;
    private TextView  assetAmount;
    private TextView  assetState;
    private TextView  assetGubn;
    //private TextView  assetBigo;

    private View.OnClickListener btnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnSearch :
                    getAssetDetail();
                    break;
                case R.id.btnScan :
                    IntentIntegrator intentIntegrator = new IntentIntegrator();
                    intentIntegrator.initiateScan(SearchAssetActivity.this);
                    break;
            }
        }
    };

    private TextView.OnEditorActionListener etListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

            getAssetDetail();
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_asset);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        if (OnbridPreference.getString(this, "useScanner").equalsIgnoreCase("2") ) {
            usbSerialHandler = new UsbSerialHandler(this);
        }

        initControll();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (OnbridPreference.getString(this, "useScanner").equalsIgnoreCase("2") ) {
            setFilters();  // Start listening notifications from UsbService
            startService(UsbService.class, usbConnection, null); // Start UsbService(if it was not started before) and Bind it
        }
    }

    @Override
    protected void onPause() {
        if (OnbridPreference.getString(this, "useScanner").equalsIgnoreCase("2") ) {
            try {
                unregisterReceiver(mUsbReceiver);
                unbindService(usbConnection);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (OnbridPreference.getString(this, "useScanner").equalsIgnoreCase("2") ) {
            try {
                unregisterReceiver(mUsbReceiver);
                unbindService(usbConnection);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        super.onDestroy();
    }


    private void initControll() {
        btnSearch = (Button)  findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(btnListener);
        btnScan = (Button)  findViewById(R.id.btnScan);
        btnScan.setOnClickListener(btnListener);

        etAssetNo    = (EditText)  findViewById(R.id.etAssetNo);
        etAssetNo.setOnEditorActionListener(etListener);

        assetImage   = (ImageView) findViewById(R.id.assetImage);
        assetNo      = (TextView)  findViewById(R.id.assetNo    );
        assetName    = (TextView)  findViewById(R.id.assetName  );
        assetPlace   = (TextView)  findViewById(R.id.assetPlace );
        assetDept    = (TextView)  findViewById(R.id.assetDept  );
        assetModel   = (TextView)  findViewById(R.id.assetModel );
        assetSpec    = (TextView)  findViewById(R.id.assetSpec);
        assetDate    = (TextView)  findViewById(R.id.assetDate  );
        assetAmount  = (TextView)  findViewById(R.id.assetAmount);
        assetState   = (TextView)  findViewById(R.id.assetState );
        assetGubn    = (TextView)  findViewById(R.id.assetGubn  );
        //assetBigo    = (TextView)  rootView.findViewById(R.id.assetBigo  );

        // 설정에 따라 재물번호 스캔/조회 버튼 활성화 처리
        if ( Integer.parseInt(OnbridPreference.getString(this, "useScanner")) > 0 ) {
            btnScan.setVisibility(View.GONE);
            //findViewById(R.id.btnSearch).setVisibility(View.GONE);
        }

    }
    private void clearControll() {
        clearControll(0);
    }
    private void clearControll(int gubn) {
        if (gubn < 1) {
            setText(etAssetNo, "");
        }
        setText(assetNo, "" );
        setText(assetName, "" );
        setText(assetPlace, "" );
        setText(assetDept, "" );
        setText(assetModel, "" );
        setText(assetSpec, "" );
        setText(assetDate, "" );
        setText(assetAmount, "" );
        setText(assetState, "" );
        setText(assetGubn, "" );
        //setText(assetBigo, "" );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_fragment_main1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch ( item.getItemId() ){
            case R.id.action_search :
                getAssetDetail();
                break;
            case R.id.action_clear :
                clearControll();
                break;
            default:
                finish();
                break;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case IntentIntegrator.REQUEST_CODE:
            {
                if (resultCode == RESULT_CANCELED){
                    Toast.makeText(this, "재물번호 바코드 스캔 취소됨", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

                    String UPCScanned = scanResult.getContents();

                    setText(etAssetNo, UPCScanned.trim());
                    getAssetDetail();
                }
                break;
            }
        }
    }

    private void getAssetDetail() {
        clearControll(1);
        try {
            if (etAssetNo.getText().toString().equalsIgnoreCase("")) {
                return;
            }

            String url = "/ws/pda/asset/select";

            // {DATABLOCK=[{UNIVNO=0001, LOGINEMP=1001, PLANNO=20150025, PDANO=101, CAMPUSNO=01, ASSETNO=201500000019}]}
            JSONObject param = new JSONObject();
            JSONArray dataBlock = new JSONArray();
            JSONObject paramInner = new JSONObject();

            paramInner.put("UNIVNO"  , OnbridPreference.getString(this, "univNo"));
            paramInner.put("CAMPUSNO", OnbridPreference.getString(this, "campusNo"));
            paramInner.put("ASSETNO" , etAssetNo.getText().toString());
            dataBlock.put(paramInner);
            param.put("DATABLOCK", dataBlock);
            // Log.d("param", param.toString());

            OnbridRestClient.post(this, url, param, new OnbridResponseHandler(this) {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Log.d("onSuccess", response.toString());
                    // {"USESTATE":0,"PRICE":42000,"USEKINDNAME":"공기구비품","ASSETSTATENAME2":null,"BIGO":null,"ASSETTYPENAME":"공기구비품","CAMPUSNO":"01","GUBUNNAME":"책상",
                    // "DEPTNAME":"기획 관리부","ASSETNO":"201500000003","SPEC":"1200*700*720","DEPTNO":"0001","ASSETSTATE":255,"BARCODE":"2015000003","QTY":1,"ASSETNAME":"책상",
                    // "NOMALGUBUN":0,"PLACENAME":"기획 관리부 사무실","NOMALGUBUNNAME":null,"GUBUNNO":"0001","MODELNAME":"703 탑책상","UNIVNO":"0001","CAMPUSNAME":"OnBrid-대학",
                    // "AMOUNT":42000,"ASSETSTATENAME":"정상","WRITEDATE":"20150907","ACCGUBUNNAME":null,"ACCGUBUN":null,"ROWNUM":1,"ASSETTYPE":"0008","OLDASSETNO":null,"USEKIND":"0016","PLACENO":"0003","DUDATE":null}
                    try {
                        setText(assetNo, response.getString("ASSETNO"));
                        setText(assetName, response.getString("ASSETNAME"));
                        setText(assetPlace, response.getString("PLACENAME"));
                        setText(assetDept, response.getString("DEPTNAME"));
                        setText(assetModel, response.getString("MODELNAME"));
                        setText(assetSpec, response.getString("SPEC"));
                        setText(assetDate, response.getString("WRITEDATE"), "date");
                        setText(assetAmount, response.getString("AMOUNT"), "decimal");
                        setText(assetState, response.getString("ASSETSTATENAME"));
                        setText(assetGubn, response.getString("ASSETTYPENAME"));
                        //setText(assetBigo, response.getString("BIGO"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Toast.makeText(SearchAssetActivity.this, "비품정보가 없습니다.", Toast.LENGTH_SHORT).show();
                }
            });


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * EditText, TextView 에 text(내용)을 셋한다.
     * @param item
     * @param val
     */
    private void setText(TextView item, Object val) {
        item.setText(val.toString().equalsIgnoreCase("null") ? "-" : val.toString());
    }
    private void setText(TextView item, Object val, String type) {
        String value = "";

        if (type.equalsIgnoreCase("date")) {
            value = val.toString().substring(0,4) + "-" + val.toString().substring(4,6) + "-" + val.toString().substring(6,8);
        } else if (type.equalsIgnoreCase("decimal")) {
            DecimalFormat format = new DecimalFormat("#,###");
            value = (String) format.format(Integer.parseInt(val.toString()));
        } else {
            value = (String) val;
        }

        setText(item, value);
    }



    /**
     * UsbSerial 스캔 이벤트 결과 처리
     * @param data
     */
    private void getHandleMessage( String data ) {
        setText(etAssetNo, data.trim());
        getAssetDetail();
    }

    /**
     * Inner Class UsbSerialHandler
     */
    private static class UsbSerialHandler extends Handler {
        private final WeakReference<SearchAssetActivity> mActivity;

        public UsbSerialHandler(SearchAssetActivity activity) { mActivity = new WeakReference<SearchAssetActivity>(activity); }

        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case UsbService.MESSAGE_FROM_SERIAL_PORT:
                    String data = ((String) msg.obj).trim();
                    if (data.length() > 0) {
                        // Log.e("USB_DATA", data + "/" + data.length());
                        mActivity.get().getHandleMessage(data);
                    }
                    break;
            }
        }
    }

    private void startService(Class<?> service, ServiceConnection serviceConnection, Bundle extras)
    {
        if(UsbService.SERVICE_CONNECTED == false)
        {
            Intent startService = new Intent(this, service);
            if(extras != null && !extras.isEmpty())
            {
                Set<String> keys = extras.keySet();
                for(String key: keys)
                {
                    String extra = extras.getString(key);
                    startService.putExtra(key, extra);
                }
            }
            startService(startService);
        }
        Intent bindingIntent = new Intent(this, service);
        bindService(bindingIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    private void setFilters()
    {
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbService.ACTION_USB_PERMISSION_GRANTED);
        filter.addAction(UsbService.ACTION_NO_USB);
        filter.addAction(UsbService.ACTION_USB_DISCONNECTED);
        filter.addAction(UsbService.ACTION_USB_NOT_SUPPORTED);
        filter.addAction(UsbService.ACTION_USB_PERMISSION_NOT_GRANTED);
        registerReceiver(mUsbReceiver, filter);
    }

    /*
	 * Notifications from UsbService will be received here.
	 */
    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context arg0, Intent arg1)
        {
            if(arg1.getAction().equals(UsbService.ACTION_USB_PERMISSION_GRANTED)) // USB PERMISSION GRANTED
            {
                Toast.makeText(arg0, "USB 시리얼 스캐너 준비됨", Toast.LENGTH_SHORT).show();
            }else if(arg1.getAction().equals(UsbService.ACTION_USB_PERMISSION_NOT_GRANTED)) // USB PERMISSION NOT GRANTED
            {
                Toast.makeText(arg0, "USB 통신 권한없음", Toast.LENGTH_SHORT).show();
            }else if(arg1.getAction().equals(UsbService.ACTION_NO_USB)) // NO USB CONNECTED
            {
                Toast.makeText(arg0, "USB 연결안됨", Toast.LENGTH_SHORT).show();
            }else if(arg1.getAction().equals(UsbService.ACTION_USB_DISCONNECTED)) // USB DISCONNECTED
            {
                Toast.makeText(arg0, "USB 분리됨", Toast.LENGTH_SHORT).show();
            }else if(arg1.getAction().equals(UsbService.ACTION_USB_NOT_SUPPORTED)) // USB NOT SUPPORTED
            {
                Toast.makeText(arg0, "USB 장치 지원안함", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private final ServiceConnection usbConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName arg0, IBinder arg1)
        {
            usbService = ((UsbService.UsbBinder) arg1).getService();
            usbService.setHandler(usbSerialHandler);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0)
        {
            usbService = null;
        }
    };

} // class
