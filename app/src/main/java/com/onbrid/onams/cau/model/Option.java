package com.onbrid.onams.cau.model;

import java.io.Serializable;

/**
 * Created by Enable on 2015-10-13.
 */
public class Option implements Serializable {

    private String no;
    private String name;

    private String type = null;
    private String typeNo = "";
    private String typeName = "";

    private int state   =   -1;
    private int draw;
    private int index   =   -1;

    private boolean required    =   false;
    private boolean visibleNo   =   true;

    public Option( String typeNo, String typeName, String type, int draw ) {
        this.typeNo   =   typeNo;
        this.typeName =   typeName;
        this.type     =   type;
        this.draw     =   draw;

        if ( this.typeName.equalsIgnoreCase("STATUS") ) {
            this.setVisibleNo(false);
        }
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
        this.setState( (this.no != null && this.no.length() != 0) ? 0 : -1 );
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeNo() {
        return typeNo;
    }

    public void setTypeNo(String typeNo) {
        this.typeNo = typeNo;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean isVisibleNo() {
        return visibleNo;
    }

    public void setVisibleNo(boolean visibleNo) {
        this.visibleNo = visibleNo;
    }

    public void set(Code code) {
        this.setNo(code.getNo());
        this.setName(code.getName());
    }

    public void clear() {
        this.setNo("");
        this.setName("");
    }
}
