package com.onbrid.onams.cau;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.onbrid.onams.cau.adaptor.AssetSearchOptionAdaptor;
import com.onbrid.onams.cau.barcodescan.IntentIntegrator;
import com.onbrid.onams.cau.barcodescan.IntentResult;
import com.onbrid.onams.cau.model.Code;
import com.onbrid.onams.cau.model.Option;
import com.onbrid.onams.cau.util.OnbridPreference;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;

public class AssetPlusInsertActivity extends AppCompatActivity {

    private Toolbar toolbar;

    private Map paramMap;

    private ListView listView;

    private EditText assetNo;
    private TextView assetName;
    private TextView modelName;
    private TextView spec;
    private TextView serialNum;
    private TextView buyDate;
    private TextView amount;
    private TextView deptName;
    private TextView placeName;

    private Button btnScan;
    private Button btnSearchAsset;

    private AssetSearchOptionAdaptor assetSearchOptionAdaptor;

    private final String OPTION_DEPT  = "DEPT";
    private final String OPTION_PLACE = "PLACE";
    private final int    REQ_PLACE    = 1;
    private final int    REQ_SCAN     = 2;

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnScan: //Log.i("listener", "btnScan");
                    IntentIntegrator intentIntegrator = new IntentIntegrator();
                    intentIntegrator.initiateScan(AssetPlusInsertActivity.this);
                    break;
                case R.id.btnSearchAsset: //Log.i("listener", "btnSearchAsset");
                    searchAssetNo();
                    break;
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset_plus_insert);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        this.initControl();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case IntentIntegrator.REQUEST_CODE:
            {
                if (resultCode == RESULT_CANCELED){
                    Toast.makeText(this, "재물번호 바코드 스캔 취소됨", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

                    String UPCScanned = scanResult.getContents();

                    assetNo.setText(UPCScanned);
                    searchAssetNo();
                }
                break;
            }
            case REQ_PLACE: // 공통코드 조회
                if (resultCode == RESULT_OK) {
                    Code returnCode = data.hasExtra("code")? (Code) data.getSerializableExtra("code") : null;
                    String type  = data.getStringExtra("type");
                    int position = data.getIntExtra("position", -1);

                    if (returnCode == null || position == -1) {
                        return;
                    }

                    ((Option) assetSearchOptionAdaptor.getItem(position)).set(returnCode);

                    switch ( type ) {
                        case OPTION_PLACE: paramMap.put("SCANPLACENO", returnCode.getNo());
                            break;
                    }
                    assetSearchOptionAdaptor.notifyDataSetChanged();

                }
                break;
            default: break;

        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_asset_plus_insert, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch ( item.getItemId() ) {

            case R.id.action_save: // Log.i("ItemSelected", "action_save");
                insertSurveyAsset();
                break;
            case R.id.action_clear: // Log.i("ItemSelected", "action_clear");
                clearControl(1);
                break;
            default:
                finish();
                break;

        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * 컨트롤러를 초기화한다.
     */
    private void initControl() {

        listView    = (ListView) findViewById(R.id.listView);
        assetNo     = (EditText) findViewById(R.id.etAssetNo);
        assetName   = (TextView) findViewById(R.id.tvAssetName);
        modelName   = (TextView) findViewById(R.id.tvModelName);
        spec        = (TextView) findViewById(R.id.tvSpec);
        serialNum   = (TextView) findViewById(R.id.tvSerialNum);
        buyDate     = (TextView) findViewById(R.id.tvBuyDate);
        amount      = (TextView) findViewById(R.id.tvAmount);
        deptName    = (TextView) findViewById(R.id.tvDeptName);
        placeName   = (TextView) findViewById(R.id.tvPlaceName);

        btnScan        = (Button) findViewById(R.id.btnScan);
        btnSearchAsset = (Button) findViewById(R.id.btnSearchAsset);
        btnScan.setOnClickListener(listener);
        btnSearchAsset.setOnClickListener(listener);

        this.initParam();
        this.initListView();
    }

    /**
     * 컨트롤의 값을 클리어한다.<br>
     * gubn이 1보다 작으면 assetNo초기화함.
     * @param gubn
     */
    private void clearControl(int gubn) {
        // 컨트롤의 값을 클리어한다.
        if (gubn > 0) {
            assetNo.setText("");
        }
        assetName.setText("");
        modelName.setText("");
        spec.setText("");
        serialNum.setText("");
        buyDate.setText("");
        amount.setText("");
        deptName.setText("");
        placeName.setText("");

        paramMap.put("SCANPLACENO", getIntent().getSerializableExtra("SCANPLACENO"));
        paramMap.put("SCANPLACENAME", getIntent().getSerializableExtra("SCANPLACENAME"));
        // if (getIntent().getSerializableExtra("SCANPLACENO").toString())
        ((Option) assetSearchOptionAdaptor.getItem(1)).set(new Code(getIntent().getSerializableExtra("SCANPLACENO").toString(), getIntent().getSerializableExtra("SCANPLACENAME").toString()));
        assetSearchOptionAdaptor.notifyDataSetChanged();
    }

    /**
     * 통신 파라미터를 초기화한다.
     */
    private void initParam() {
        if (paramMap == null) {
            paramMap = new HashMap();
        }
        paramMap.put("UNIVNO", OnbridPreference.getString(AssetPlusInsertActivity.this, "univNo"));
        paramMap.put("CAMPUSNO", OnbridPreference.getString(AssetPlusInsertActivity.this, "campusNo"));
        paramMap.put("LOGINEMP", OnbridPreference.getString(AssetPlusInsertActivity.this, "loginEmp"));
        paramMap.put("PLANNO", getIntent().getSerializableExtra("PLANNO"));
        paramMap.put("SCANDEPTNO", getIntent().getSerializableExtra("SCANDEPTNO"));
        paramMap.put("SCANDEPTNAME", getIntent().getSerializableExtra("SCANDEPTNAME"));
        paramMap.put("SCANPLACENO", getIntent().getSerializableExtra("SCANPLACENO"));
        paramMap.put("SCANPLACENAME", getIntent().getSerializableExtra("SCANPLACENAME"));


    }

    /**
     * 장소 입력정보를 받기위해 팝업 정보를 초기화한다.
     */
    private void initListView() {
        ArrayList<Option> options = new ArrayList<Option>();
        options.add(new Option("0001", "조사부서", OPTION_DEPT , R.drawable.ic_action_group));
        options.add(new Option("0002", "조사장소", OPTION_PLACE, R.drawable.ic_action_place));

        options.get(0).setRequired(true);
        options.get(1).setRequired(true);

        /* 검색조건 리스트뷰/아답터 초기화 */
        assetSearchOptionAdaptor = new AssetSearchOptionAdaptor(AssetPlusInsertActivity.this, options);
        listView.setAdapter(assetSearchOptionAdaptor);

        /* 검색조건 리스트 클릭 이벤트 */
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position < 1) {
                    Toast.makeText(getApplicationContext(), "조사부서 정보를 수정할 수 없습니다.", Toast.LENGTH_LONG).show();
                    return;
                }
                Option option = (Option) listView.getItemAtPosition(position);

                Intent intent = new Intent(AssetPlusInsertActivity.this, SearchCode.class);
                intent.putExtra("position", position);
                intent.putExtra("type", option.getType());
                startActivityForResult(intent, REQ_PLACE);
            }
        });
        ((Option) assetSearchOptionAdaptor.getItem(0)).set(new Code(getIntent().getSerializableExtra("SCANDEPTNO").toString(), getIntent().getSerializableExtra("SCANDEPTNAME").toString()));
        ((Option) assetSearchOptionAdaptor.getItem(1)).set(new Code(getIntent().getSerializableExtra("SCANPLACENO").toString(), getIntent().getSerializableExtra("SCANPLACENAME").toString()));
        assetSearchOptionAdaptor.notifyDataSetChanged();

    }

    /**
     * 네트워크 사용 유무를 체크한다.
     * @return
     */
    private boolean getNetWorkState() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }

        return false;
    }

    /**
     * 재물번호를 가지고 상세목록을 조회한다.
     */
    private void searchAssetNo() {
        if (assetNo.getText().toString().length() < 1) {
            Toast.makeText(getApplicationContext(), "재물번호를 입력해주세요.", Toast.LENGTH_LONG).show();
            return;
        }
        this.clearControl(0);

        if (getNetWorkState()) {
            try {
                String url = OnbridPreference.getString(this, "url") + "/ws/am/assetManage/selectAssetDetail";
                Map param = new HashMap();
                param.put("UNIVNO", OnbridPreference.getString(this, "univNo"));
                param.put("CAMPUSNO", OnbridPreference.getString(this, "campusNo"));
                param.put("ASSETNO", assetNo.getText().toString());
                param.put("LOGINEMP", OnbridPreference.getString(this, "loginEmp"));

                String _message = new Gson().toJson(param);

                ByteArrayEntity entity = new ByteArrayEntity(_message.getBytes("UTF-8"));

                AsyncHttpClient client = new AsyncHttpClient();
                client.post(getParent(), url, entity, "application/json", new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        // super.onSuccess(statusCode, headers, response);
                        try {
                            //Log.e("checkLogin", response.toString());
                            if (response.length() < 1) {
                                Toast.makeText(getApplicationContext(), "재물정보가 없습니다.", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            paramMap.put("DEPTNO" , response.get("DEPTNO").toString());
                            paramMap.put("PLACENO", response.get("PLACENO").toString());

                            // if (response.get("ASSETNAME").toString() != "null") etProdNm.setText(response.get("ASSETNAME").toString());
                            if (response.get("ASSETNAME").toString() != "null") assetName.setText(response.get("ASSETNAME").toString());
                            if (response.get("MODELNAME").toString() != "null") modelName.setText(response.get("MODELNAME").toString());
                            if (response.get("SPEC").toString()      != "null") spec.setText(response.get("SPEC").toString());
                            if (response.get("SERIALNUM").toString() != "null") serialNum.setText(response.get("SERIALNUM").toString());
                            if (response.get("RECORDDATE").toString() != "null") buyDate.setText(response.get("RECORDDATE").toString());
                            if (response.get("AMOUNT").toString() != "null") amount.setText(response.get("AMOUNT").toString());
                            if (response.get("DEPTNAME").toString() != "null") deptName.setText(response.get("DEPTNAME").toString());
                            if (response.get("PLACENAME").toString() != "null") placeName.setText(response.get("PLACENAME").toString());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }


                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        //super.onFailure(statusCode, headers, responseString, throwable);
                        Toast.makeText(getApplicationContext(), "재물정보가 없습니다.", Toast.LENGTH_SHORT).show();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "네트워크 연결상태를 확인해 주세요.", Toast.LENGTH_LONG).show();
        }






    }

    /**
     * 추가조사를 저장한다.
     */
    private void insertSurveyAsset() {
        //updateReadGubn
        if (assetName.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(this, "재물을 조회 후 추가할 수 있습니다.", Toast.LENGTH_SHORT).show();
            return;
        }
        if (paramMap.get("SCANPLACENO").toString().equalsIgnoreCase("")) {
            Toast.makeText(this, "조사장소를 입력하세요.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (getNetWorkState()) {
            try {
                String url = OnbridPreference.getString(this, "url") + "/ws/sa/surveyDept/updateReadGubn";
                Map param = paramMap;
                param.put("ASSETNO"         , assetNo.getText().toString());
                param.put("SCANEMPNO"       , OnbridPreference.getString(AssetPlusInsertActivity.this, "loginEmp"));
                param.put("USEEMPNO"        , "");
                param.put("ACTIONGUBN"      , 1);
                param.put("READGUBN"        , 4);
                param.put("SURVEYGUBN"      , 0);
                param.put("DEPTSURVEYGUBN"  , 0);
                param.put("SURVEYASSETSTATE", 1);
                param.put("BIGO"            , "");
                // Log.e("param", param.toString());

                String _message = new Gson().toJson(param);

                ByteArrayEntity entity = new ByteArrayEntity(_message.getBytes("UTF-8"));

                AsyncHttpClient client = new AsyncHttpClient();
                client.post(getParent(), url, entity, "application/json", new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        // super.onSuccess(statusCode, headers, response);
                        try {
                            Log.e("onSuccess", response.toString());

                            Toast.makeText(getApplicationContext(), "추가조사 되었습니다.", Toast.LENGTH_SHORT).show();

                            clearControl(1);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }


                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        //super.onFailure(statusCode, headers, responseString, throwable);
                        Toast.makeText(getApplicationContext(), "재물정보가 없습니다.", Toast.LENGTH_SHORT).show();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "네트워크 연결상태를 확인해 주세요.", Toast.LENGTH_LONG).show();
        }

    }









}
