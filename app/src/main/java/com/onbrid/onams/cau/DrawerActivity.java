package com.onbrid.onams.cau;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.onbrid.onams.cau.fragment.MarketAsset;
import com.onbrid.onams.cau.fragment.MenuList;
import com.onbrid.onams.cau.fragment.SearchAsset;
import com.onbrid.onams.cau.fragment.SurveyAsset;
import com.onbrid.onams.cau.fragment.SurveyAssetBase;
import com.onbrid.onams.cau.fragment.SurveyAssetScan;
import com.onbrid.onams.cau.util.OnbridPreference;

import java.util.HashMap;
import java.util.Map;

public class DrawerActivity extends ActionBarActivity {

    private Toolbar toolbar;
    private ActionBarDrawerToggle actionBarDrawerToggle;

    private ListView listView;
    private FrameLayout frameLayout;
    private DrawerLayout drawerLayout;

    // private String[] items = { "메인", "재물조회", "부서재물조사", "기초재물조사"};
    // private String[] items = { "메인", "재물조회", "부서재물조사(체크)", "부서재물조사(Scan)", "기초재물조사", "재고활용"};
    private String[] items = { "메인", "재물조회", "재물조사_스캔", "재물조사_체크", "기초 재물조사"};
    private boolean isDrawerOpen = false;
    private int currPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        listView    = (ListView) findViewById(R.id.lv_activity_main_nav_list);
        frameLayout = (FrameLayout) findViewById(R.id.fl_activity_main_container);
        drawerLayout = (DrawerLayout) findViewById(R.id.dl_activity_main_drawer);

        // 상단 액션바 설정
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // 이벤트 생성.
        actionBarDrawerToggle = new ActionBarDrawerToggle( this
                                                         , drawerLayout
                                                         , R.string.abc_action_mode_done
                                                         , R.string.abc_action_mode_done ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                isDrawerOpen = false;
                Log.i("", "onDrawerClosed");
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                isDrawerOpen = true;
                Log.i("", "onDrawerOpened");
            }
        };
        // drawerLayout 이벤트 연결
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        // 네비게이션 리스트 클릭 이벤트 연결
        listView.setAdapter( new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Log.i("", "listView onItemClick");
                drawerLayout.closeDrawers();
                setFragments(position);
            }
        });

        // ??
        drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                actionBarDrawerToggle.syncState();
            }
        });



    } // onCreate

    @Override
    protected void onStart() {
        super.onStart();
        // Fragment 를 세팅한다.
        if (currPosition == -1) {
            setFragments(0);
        } else {
            setFragments(currPosition);
        }
    }

    /**
     * 밖으로 나가게 하지 않기 위해 ..
     * 없다면 로그인 화면으로 나가버린다..
     */
    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        if (isDrawerOpen == true) {
            drawerLayout.closeDrawers();
        } else {
            setFragments(0);
        }
        Log.e("DrawerActivity", "DrawerActivity.onBackPressed");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if( actionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setFragments(int position) {

        if (currPosition == position) return;

        Log.e("sectionPosi", ""+position);

        if ( position == 1) {
            Intent intent = new Intent(DrawerActivity.this, SearchAssetActivity.class);
            startActivity(intent);
            return;
        }


        if ( position == 2 ) {
            if (OnbridPreference.getString(this, "planNo") == null
                    || OnbridPreference.getString(this, "planNo").equalsIgnoreCase("")
                    || OnbridPreference.getString(this, "planNo").toUpperCase().equalsIgnoreCase("NULL")) {
                Toast.makeText(this, "진행중인 재물조사가 없습니다.", Toast.LENGTH_LONG).show();
                return;
            }
        }

        currPosition = position;

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                //.setCustomAnimations(R.anim.abc_slide_in_top, R.anim.abc_slide_out_bottom)
                .replace(R.id.fl_activity_main_container, PlaceholderFragment.newInstance(this, position + 1))
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();

        listView.setItemChecked(position, true);
        ((ArrayAdapter) listView.getAdapter()).notifyDataSetChanged();

    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {}

        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */

        private static Map<String, Fragment> fragmentMap = new HashMap<String, Fragment>();

        private static int currentSectionNumber = -1;

        public static Fragment newInstance(Context context, int sectionNumber) {

            currentSectionNumber = sectionNumber;

            // PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);

            Fragment fragment = null;

            if ( fragmentMap.containsKey(sectionNumber+"") ) {
                fragment = fragmentMap.get(sectionNumber+"");
            } else {
                switch ( sectionNumber - 1 ) {
                    case 0 : fragment = new MenuList(); break;
                    case 1 : fragment = new SearchAsset(); break;
                    case 2 : fragment = new SurveyAssetScan(); break;
                    case 3 : fragment = new SurveyAsset(); break;
                    case 4 : fragment = new SurveyAssetBase(); break;
                    case 5 : fragment = new MarketAsset(); break;
                    default: fragment = new PlaceholderFragment(); break;
                }
                fragmentMap.put(sectionNumber + "", fragment);
            }

            fragment.setArguments(args);

            return fragment;
        }

        public static int getCurrentFragmentIndex() { return currentSectionNumber; }
        public static Fragment getFragment() { return fragmentMap.get(currentSectionNumber+""); }


    } // inner class

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
        System.out.println("DrawerActivity onConfigurationChanged");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        System.out.println("DrawerActivity onCreateOptionsMenu");
        return super.onCreateOptionsMenu(menu);
    }


} // class
